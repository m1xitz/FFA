package de.m0xitz.ffa.scenario;

import com.google.common.collect.Maps;
import de.m0xitz.ffa.Main;
import de.slikey.effectlib.util.ParticleEffect;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class Scenario implements Runnable {

    private Main plugin;

    private Random random;
    private List< Location > locations;

    private int count;
    private BukkitTask countId;
    private BukkitTask effectId;

    private boolean exists;

    public Scenario( Main plugin ) {
        this.plugin = plugin;

        random = new Random( );
        locations = plugin.getLocationManager( ).getZombieScenario( );

        count = 10;

        exists = false;

        countId = plugin.runTaskTimer( 0, 20, this );
        startEffectCount( exists );
    }

    @Override
    public void run( ) {
    }


    //<editor-fold desc="startEffectCount">
    private void startEffectCount( boolean tornado ) {
        if ( effectId != null )
            effectId.cancel( );

        if ( tornado ) {
            effectId = plugin.runTaskTimer( 0, 30, new Runnable( ) {
                HashMap< Integer, BukkitTask > maps = Maps.newHashMap( );

                @Override
                public void run( ) {
                    for ( Location location : locations ) {
                        location = location.clone( ).add( 0.5, 0.0, 0.5 );
                        startTornado( location );
                    }
                    return;
                }

                //<editor-fold desc="startTornado">
                public void startTornado( Location location ) {
                    int id = random.nextInt( 999999999 );
                    if ( maps.containsKey( id ) )
                        id = random.nextInt( 999999999 );
                    int finalId = id;
                    maps.put( id, plugin.runTaskTimer( 0, 1, new Runnable( ) {
                        Location loc = location.clone( ).add( 0.0, 4.0, 0.0 );

                        int i = 0;
                        int e = 0;
                        double r = 1.5;
                        int f = 0;

                        @Override
                        public void run( ) {
                            if ( !exists ) {
                                maps.get( finalId ).cancel( );
                                maps.remove( finalId );
                                return;
                            }
                            List< Location > locs = plugin.getLocationManager( ).getCircle( loc, this.r, 50 );
                            ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 1, locs.get( this.i ), 100.0 );
                            i++;
                            e++;
                            f++;
                            if ( this.i == 50 ) {
                                this.i = 0;
                            }
                            loc.add( 0.0, -0.02, 0.0 );
                            if ( this.e == 7 ) {
                                this.e = 0;
                                this.r -= 0.05;
                            }
                            if ( this.f == 207 ) {
                                maps.get( finalId ).cancel( );
                                maps.remove( finalId );
                            }
                        }
                    } ) );
                }
                //</editor-fold>
            } );
            return;
        }

        effectId = plugin.runTaskTimer( 0, 2, ( ) -> {
            for ( Location location : locations ) {
                location = location.clone( ).add( 0.5, 0.1, 0.5 );
                ParticleEffect.CRIT.display( 0.0f, 0.0f, 0.0f, 0.0f, 1, location, 100.0 );
            }
        } );
    }
    //</editor-fold>

    //<editor-fold desc="spawn">
    public void start( ) {
        exists = true;
        locations.get( 0 ).getWorld( ).setTime( 20000 );
        startEffectCount( exists );
    }
    //</editor-fold>

    //<editor-fold desc="stop">
    public void stop( ) {
        exists = false;
        locations.get( 0 ).getWorld( ).setTime( 6000 );
        startEffectCount( exists );
    }
    //</editor-fold>

    //<editor-fold desc="exists">
    public boolean exists( ) {
        return exists;
    }
    //</editor-fold>
}

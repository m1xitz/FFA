package de.m0xitz.ffa.scenario;

import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.Command;
import org.bukkit.entity.Player;

public class ScenarioManager {

    private Main plugin;

    private Scenario scenario;

    public ScenarioManager( Main plugin ) {
        this.plugin = plugin;

        if ( plugin.getLocationManager( ).getZombieScenario( ).size( ) == 0 )
            return;

        scenario = new Scenario( plugin );
        plugin.getEventManager( ).onCommand( new Command( "lol" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( scenario.exists( ) )
                    scenario.stop( );
                else
                    scenario.start( );
            }
        } );
    }

    //<editor-fold desc="getScenario">
    public Scenario getScenario( ) {
        return scenario;
    }
    //</editor-fold>
}

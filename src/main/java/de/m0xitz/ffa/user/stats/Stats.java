package de.m0xitz.ffa.user.stats;

import java.text.NumberFormat;

public interface Stats {

    long getKills( );

    long getDeaths( );

    long getPoints( );

    long getJoins( );

    long getHighestKillstreak( );

    default String getKD( ) {
        double kd = ( double ) getKills( ) / ( double ) getDeaths( );
        NumberFormat numberFormat = NumberFormat.getInstance( );
        numberFormat.setMaximumFractionDigits( 2 );
        return numberFormat.format( kd )
                .replace( ".", "" )
                .replace( ",", "." );
    }
}

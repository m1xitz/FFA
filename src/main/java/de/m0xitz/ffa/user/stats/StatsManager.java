package de.m0xitz.ffa.user.stats;

import com.google.common.collect.Maps;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.Command;
import de.m0xitz.ffa.user.User;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class StatsManager {

    private Main plugin;

    private HashMap< UUID, Stats > statsCache;

    public StatsManager( Main plugin ) {
        this.plugin = plugin;

        statsCache = Maps.newHashMap( );
        init( );
    }

    private void init( ) {
        //<editor-fold desc="StatsCommand">
        plugin.getEventManager( ).onCommand( new Command( "stats" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( strings.length == 1 ) {
                    UUID uuid = plugin.getServer( ).getPlayerExact( strings[0] ) != null ? plugin.getServer( ).getPlayerExact( strings[0] ).getUniqueId( ) : null; // TODO: ADD UUIDFETCHER
                    if ( uuid == null ) {
                        player.sendMessage( plugin.getPrefix( ) + "§cDer Spieler §e" + strings[0] + " §cwar noch nie auf dem Netzwerk§8." );
                        return;
                    }
                    sendStats( player, uuid );
                    return;
                }
                sendStats( player, player.getUniqueId( ) );
            }
        } );
        //</editor-fold>
        //<editor-fold desc="StatsresetCommand">
        plugin.getEventManager( ).onCommand( new Command( "statsreset" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( !player.hasPermission( "ffa.command.statsreset" ) ) {
                    player.sendMessage( plugin.getPrefix( ) + "§cFür diesen Command hast du keine Rechte!" );
                    return;
                }
                if ( strings.length == 1 ) {
                    UUID uuid = plugin.getServer( ).getPlayerExact( strings[0] ) != null ? plugin.getServer( ).getPlayerExact( strings[0] ).getUniqueId( ) : null; // TODO: ADD UUIDFETCHER
                    if ( uuid == null ) {
                        player.sendMessage( plugin.getPrefix( ) + "§cDer Spieler §e" + strings[0] + " §cwar noch nie auf dem Netzwerk§8." );
                        return;
                    }
                    Stats stats = new Stats( ) {
                        @Override
                        public long getKills( ) {
                            return 0;
                        }

                        @Override
                        public long getDeaths( ) {
                            return 0;
                        }

                        @Override
                        public long getPoints( ) {
                            return 100;
                        }

                        @Override
                        public long getJoins( ) {
                            return 0;
                        }

                        @Override
                        public long getHighestKillstreak( ) {
                            return 0;
                        }
                    };
                    if ( plugin.getServer( ).getPlayer( uuid ) == null ) {
                        plugin.getDatabaseManager( ).getDatabase( ).setStats( uuid, stats );
                        player.sendMessage( plugin.getPrefix( ) + "§7Du hast erfolgreich die Stats von §e" + plugin.getServer( ).getOfflinePlayer( uuid ).getName( ) + " §7resetet§8." );

                        return;
                    }
                    User ffaUser = plugin.getUserManager( ).getUser( uuid );
                    ffaUser.setStats( stats );
                    player.sendMessage( plugin.getPrefix( ) + "§7Du hast erfolgreich die Stats von §e" + ffaUser.getPlayer( ).getName( ) + " §7resetet§8." );
                    return;
                }
                player.sendMessage( plugin.getPrefix( ) + "§cNutze§8: §8/§cstatsreset §7[§fSpieler§7]" );
            }
        } );
        //</editor-fold>
    }

    //<editor-fold desc="sendStats">
    public void sendStats( Player player, UUID uuid ) {
        Stats stats = plugin.getUserManager( ).selectStats( uuid );
        String name = plugin.getServer( ).getPlayer( uuid ) != null ? plugin.getServer( ).getPlayer( uuid ).getName( ) : "Oswald"; // TODO: ADD UUIDFLETCHER

        player.sendMessage( "§8§m-----------------------------" );
        player.sendMessage( plugin.getPrefix( ) + "Stats von §6" + name );
        player.sendMessage( plugin.getPrefix( ) + "§7Ranking §8➟ §e" + plugin.getDatabaseManager( ).getDatabase( ).getRanking( uuid ) );
        player.sendMessage( plugin.getPrefix( ) + "§7Kills §8➟ §e" + stats.getKills( ) );
        player.sendMessage( plugin.getPrefix( ) + "§7Deaths §8➟ §e" + stats.getDeaths( ) );
        player.sendMessage( plugin.getPrefix( ) + "§7KD §8➟ §e" + stats.getKD( ) );
        player.sendMessage( plugin.getPrefix( ) + "§7Points §8➟ §e" + stats.getPoints( ) );
        player.sendMessage( plugin.getPrefix( ) + "§7Beste Killstreak §8➟ §e" + stats.getHighestKillstreak( ) );
        player.sendMessage( plugin.getPrefix( ) + "§7FFA beigetreten §8➟ §e" + stats.getJoins( ) );
        player.sendMessage( "§8§m-----------------------------" );
    }
    //</editor-fold>

    //<editor-fold desc="addToCache">
    public void addToCache( UUID uuid, Stats stats ) {
        statsCache.put( uuid, stats );
    }
    //</editor-fold>

    //<editor-fold desc="removeFromCache">
    public void removeFromCache( UUID uuid ) {
        statsCache.remove( uuid );
    }
    //</editor-fold>

    //<editor-fold desc="isInCache">
    public boolean isInCache( UUID uuid ) {
        return statsCache.containsKey( uuid );
    }
    //</editor-fold>

    //<editor-fold desc="getCacheStats">
    public Stats getCacheStats( UUID uuid ) {
        return statsCache.get( uuid );
    }
    //</editor-fold>
}

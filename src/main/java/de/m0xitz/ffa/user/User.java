package de.m0xitz.ffa.user;

import com.comphenix.packetwrapper.AbstractPacket;
import com.comphenix.packetwrapper.WrapperPlayServerRespawn;
import com.comphenix.packetwrapper.WrapperPlayServerUpdateHealth;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.event.KillstreakChangeEvent;
import de.m0xitz.ffa.misc.ScoreboardBuilder;
import de.m0xitz.ffa.user.inventory.Inventory;
import de.m0xitz.ffa.user.stats.Stats;
import net.minecraft.server.v1_8_R3.Packet;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

public class User {

    private Main plugin;
    private Player player;
    private CraftPlayer craftPlayer;

    private Stats stats;
    private Inventory inventory;

    private Player lastDamager;

    private long kills;
    private long deaths;
    private long points;
    private long joins;
    private long highestKillstreak;

    private long killstreak;

    private ScoreboardBuilder scoreboard;

    public User( Main plugin, Player player ) {
        this.plugin = plugin;
        this.player = player;

        craftPlayer = ( CraftPlayer ) player;
        scoreboard = new ScoreboardBuilder( this, plugin.getAnimatedScoreboard( ).getState( ) );
    }

    //<editor-fold desc="setStats">
    public void setStats( Stats stats ) {
        this.stats = stats;

        kills = stats.getKills( );
        deaths = stats.getDeaths( );
        points = stats.getPoints( );
        joins = stats.getJoins( ) + 1;
        highestKillstreak = stats.getHighestKillstreak( );

        setScoreboard( );
    }
    //</editor-fold>

    //<editor-fold desc="getStats">
    public Stats getStats( ) {
        return new Stats( ) {
            @Override
            public long getKills( ) {
                return kills;
            }

            @Override
            public long getDeaths( ) {
                return deaths;
            }

            @Override
            public long getPoints( ) {
                return points;
            }

            @Override
            public long getJoins( ) {
                return joins;
            }

            @Override
            public long getHighestKillstreak( ) {
                return highestKillstreak;
            }
        };
    }
    //</editor-fold>

    //<editor-fold desc="setInventory">
    public void setInventory( Inventory inventory ) {
        this.inventory = inventory;
    }
    //</editor-fold>

    //<editor-fold desc="getInventory">
    public Inventory getInventory( ) {
        return inventory;
    }
    //</editor-fold>

    //<editor-fold desc="addKill">
    public void addKill( ) {
        kills++;
        setLine( 10, "§8➟ §a" + kills );

        KillstreakChangeEvent event = new KillstreakChangeEvent( player, KillstreakChangeEvent.Action.ADD, killstreak, killstreak + 1 );
        plugin.getServer( ).getPluginManager( ).callEvent( event );
        killstreak++;
    }
    //</editor-fold>

    //<editor-fold desc="addDeath">
    public void addDeath( ) {
        deaths++;
        setLine( 7, "§8➟ §c" + deaths );

        KillstreakChangeEvent event = new KillstreakChangeEvent( player, KillstreakChangeEvent.Action.CLEAR, killstreak, 0 );
        plugin.getServer( ).getPluginManager( ).callEvent( event );
        killstreak = 0;
    }
    //</editor-fold>

    //<editor-fold desc="setHighestKillstreak">
    public void setHighestKillstreak( long highestKillstreak ) {
        this.highestKillstreak = highestKillstreak;
    }
    //</editor-fold>

    //<editor-fold desc="setPoints">
    public void setPoints( long points ) {
        if ( points < 0 )
            points = 0;

        this.points = points;
        setLine( 4, "§8➟ §e" + points );
    }
    //</editor-fold>

    //<editor-fold desc="setScoreboard">
    public void setScoreboard( ) {
        scoreboard.setLine( 12, "§1" );
        scoreboard.setLine( 11, "§7Kills§8:" );
        scoreboard.setLine( 10, "§8➟ §a" + stats.getKills( ) );
        scoreboard.setLine( 9, "§2" );
        scoreboard.setLine( 8, "§7Deaths§8:" );
        scoreboard.setLine( 7, "§8➟ §c" + stats.getDeaths( ) );
        scoreboard.setLine( 6, "§3" );
        scoreboard.setLine( 5, "§7Points§8:" );
        scoreboard.setLine( 4, "§8➟ §e" + stats.getPoints( ) );
        scoreboard.setLine( 3, "§4" );
        scoreboard.setLine( 2, "§7Supply Drop§8:" );
        scoreboard.setLine( 1, "§8➟ §d" + ( plugin.getSupplyDropManager( ).getSupplyDrop( ).exists( ) ? "Gespawnt §a✔" : plugin.getSupplyDropManager( ).getSupplyDrop( ).getCount( ) ) );
        scoreboard.setup( );
    }
    //</editor-fold>

    //<editor-fold desc="getScoreboard">
    public ScoreboardBuilder getScoreboard( ) {
        return scoreboard;
    }
    //</editor-fold>

    //<editor-fold desc="setLine">
    public void setLine( int line, String text ) {
        scoreboard.setLine( line, text );
    }
    //</editor-fold>

    //<editor-fold desc="sendActionbar">
    public void sendActionbar( String message ) {
        plugin.getPlayerManager( ).sendActionbar( player, message );
    }
    //</editor-fold>

    //<editor-fold desc="getPlayer">
    public Player getPlayer( ) {
        return player;
    }

    public CraftPlayer getCraftPlayer( ) {
        return craftPlayer;
    }

    //</editor-fold>

    //<editor-fold desc="sendPacket">
    public void sendPacket( Packet< ? > packet ) {
        craftPlayer.getHandle( ).playerConnection.sendPacket( packet );
    }

    public void sendPacket( PacketContainer packetContainer ) {
        try {
            plugin.getProtocolManager( ).sendServerPacket( player, packetContainer );
        } catch ( InvocationTargetException e ) {
            e.printStackTrace( );
        }
    }

    public void sendPacket( AbstractPacket abstractPacket ) {
        abstractPacket.sendPacket( player );
    }
    //</editor-fold>

    //<editor-fold desc="respawn">
    public void respawn( ) {
        Location location = player.getLocation( ).clone( );
        boolean allowFlight = player.getAllowFlight( );
        boolean flying = player.isFlying( );
        int slot = player.getInventory( ).getHeldItemSlot( );

        WrapperPlayServerRespawn respawnPacket = new WrapperPlayServerRespawn( );
        respawnPacket.setDimension( player.getWorld( ).getEnvironment( ).getId( ) );
        respawnPacket.setDifficulty( EnumWrappers.Difficulty.valueOf( player.getWorld( ).getDifficulty( ).toString( ) ) );
        respawnPacket.setGamemode( EnumWrappers.NativeGameMode.fromBukkit( player.getGameMode( ) ) );
        respawnPacket.setLevelType( player.getWorld( ).getWorldType( ) );

        WrapperPlayServerUpdateHealth healthUpdatePacket = new WrapperPlayServerUpdateHealth( );
        healthUpdatePacket.setFood( player.getFoodLevel( ) );
        healthUpdatePacket.setFoodSaturation( player.getSaturation( ) );
        healthUpdatePacket.setHealth( ( float ) player.getHealth( ) );

        sendPacket( respawnPacket );
        sendPacket( healthUpdatePacket );
        player.teleport( location );
        player.updateInventory( );
        player.getInventory( ).setHeldItemSlot( slot );
        player.setAllowFlight( allowFlight );
        player.setFlying( flying );
    }

    public void realRespawn( ) {
        this.player.spigot( ).respawn( );
    }
    //</editor-fold>

    //<editor-fold desc="setLastDamager">
    public void setLastDamager( Player lastDamager ) {
        this.lastDamager = lastDamager;
    }
    //</editor-fold>

    //<editor-fold desc="getLastDamager">
    public Player getLastDamager( ) {
        return lastDamager;
    }
    //</editor-fold>

    //<editor-fold desc="hasLastDamager">
    public boolean hasLastDamager( ) {
        return lastDamager != null;
    }
    //</editor-fold>
}

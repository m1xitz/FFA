package de.m0xitz.ffa.user.inventory;

public interface Inventory {

    int getSword( );

    int getRod( );

    int getBow( );

    int getCompass( );

    int getFns( );

}

package de.m0xitz.ffa.user.inventory;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.mojang.authlib.properties.Property;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.EventListener;
import de.m0xitz.ffa.misc.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryManager {

    private Main plugin;

    private ItemBuilder sword;
    private ItemBuilder rod;
    private ItemBuilder bow;
    private ItemBuilder compass;
    private ItemBuilder fns;

    private ItemBuilder head;
    private ItemBuilder chest;
    private ItemBuilder leggigns;
    private ItemBuilder boots;

    private ItemBuilder inventoryItem;
    private ItemBuilder specItem;
    private ItemBuilder leaveItem;

    public InventoryManager( Main plugin ) {
        this.plugin = plugin;

        //<editor-fold desc="Items">
        sword = new ItemBuilder( Material.STONE_SWORD );
        sword.setDisplayName( "§7Schwert" );
        sword.setUnbreakable( true );

        rod = new ItemBuilder( Material.FISHING_ROD );
        rod.setDisplayName( "§7Angel" );
        rod.setUnbreakable( true );

        bow = new ItemBuilder( Material.BOW );
        bow.setDisplayName( "§7Bogen" );
        bow.setUnbreakable( true );

        compass = new ItemBuilder( Material.COMPASS );
        compass.setDisplayName( "§7Zielsucher" );

        fns = new ItemBuilder( Material.FLINT_AND_STEEL );
        fns.setDisplayName( "§7Feuerzeug" );


        head = new ItemBuilder( Material.IRON_HELMET );
        head.setDisplayName( "§7Helm" );
        head.setUnbreakable( true );

        chest = new ItemBuilder( Material.IRON_CHESTPLATE );
        chest.setDisplayName( "§7Brustplatte" );
        chest.setUnbreakable( true );

        leggigns = new ItemBuilder( Material.IRON_LEGGINGS );
        leggigns.setDisplayName( "§7Hose" );
        leggigns.setUnbreakable( true );

        boots = new ItemBuilder( Material.IRON_BOOTS );
        boots.setDisplayName( "§7Schuhe" );
        boots.setUnbreakable( true );


        inventoryItem = new ItemBuilder( Material.CHEST );
        inventoryItem.setDisplayName( "§8» §aInventarsotierung §8× §7Rechtsklick §8«" );

        specItem = new ItemBuilder( Material.NETHER_STAR );
        specItem.setDisplayName( "§8» §bSpectaton §8× §7Rechtsklick §8«" );

        leaveItem = new ItemBuilder( Material.SKULL_ITEM );
        leaveItem.setDisplayName( "§8» §cVerlassen §8× §7Rechtsklick §8«" );
        leaveItem.setOwner( new Property( "textures", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWI2ZjFhMjViNmJjMTk5OTQ2NDcyYWVkYjM3MDUyMjU4NGZmNmY0ZTgzMjIxZTU5NDZiZDJlNDFiNWNhMTNiIn19fQ==" ) );
        //</editor-fold>

        init( );
    }

    //<editor-fold desc="init">
    private void init( ) {
        //<editor-fold desc="PlayerInteractEvent">
        plugin.getEventManager( ).registerListener( PlayerInteractEvent.class, ( EventListener< PlayerInteractEvent > ) ( PlayerInteractEvent event ) -> {
            if ( !plugin.getPlayerManager( ).checkPlayerInteractEvent( event ) )
                return;
            Player player = event.getPlayer( );
            Action action = event.getAction( );
            if ( !( action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK ) )
                return;
            ItemStack itemStack = player.getItemInHand( );
            ItemMeta itemMeta = itemStack.getItemMeta( );
            if ( itemMeta.getDisplayName( ).equals( "§8» §aInventarsotierung §8× §7Rechtsklick §8«" ) ) {
                event.setCancelled( true );
                openSortInventory( player );
                return;
            }
            if ( itemMeta.getDisplayName( ).equals( "§8» §bSpectaton §8× §7Rechtsklick §8«" ) ) {
                event.setCancelled( true );
                if ( plugin.getSpectatorManager( ).isSpectator( player ) ) {
                    plugin.getSpectatorManager( ).removeSpectator( player, false );
                    return;
                }
                plugin.getSpectatorManager( ).setSpectator( player );
                return;
            }
            if ( itemMeta.getDisplayName( ).equals( "§8» §cVerlassen §8× §7Rechtsklick §8«" ) ) {
                event.setCancelled( true );
                sendToFallback( player );
                return;
            }
            if ( !itemMeta.getDisplayName( ).equals( "§7Zielsucher" ) )
                return;
            event.getPlayer( ).setCompassTarget( plugin.getSupplyDropManager( ).getSupplyDrop( ).getLocation( ) );
        } );
        //</editor-fold>
        //<editor-fold desc="InventoryCloseEvent">
        plugin.getEventManager( ).registerListener( InventoryCloseEvent.class, ( EventListener< InventoryCloseEvent > ) ( InventoryCloseEvent event ) -> {
            if ( !( event.getPlayer( ) instanceof Player ) )
                return;
            Player player = ( Player ) event.getPlayer( );
            if ( event.getInventory( ) == null )
                return;
            org.bukkit.inventory.Inventory inventory = event.getInventory( );
            if ( !inventory.getName( ).equals( "§9FFA §8➟ §7Inventarsotierung" ) )
                return;

            int sword[] = { 0 };
            int rod[] = { 1 };
            int bow[] = { 2 };
            int compass[] = { 7 };
            int fns[] = { 8 };

            for ( int slot = 0; slot < inventory.getSize( ); slot++ ) {
                if ( inventory.getItem( slot ) == null )
                    continue;

                ItemStack itemStack = inventory.getItem( slot );
                if ( itemStack.getItemMeta( ) == null )
                    continue;
                ItemMeta itemMeta = itemStack.getItemMeta( );
                if ( itemMeta.getDisplayName( ).equals( "§7Schwert" ) ) {
                    sword[0] = slot;
                    continue;
                }
                if ( itemMeta.getDisplayName( ).equals( "§7Angel" ) ) {
                    rod[0] = slot;
                    continue;
                }
                if ( itemMeta.getDisplayName( ).equals( "§7Bogen" ) ) {
                    bow[0] = slot;
                    continue;
                }
                if ( itemMeta.getDisplayName( ).equals( "§7Zielsucher" ) ) {
                    compass[0] = slot;
                    continue;
                }
                if ( itemMeta.getDisplayName( ).equals( "§7Feuerzeug" ) ) {
                    fns[0] = slot;
                    continue;
                }
            }

            plugin.getUserManager( ).getUser( player ).setInventory( new Inventory( ) {
                @Override
                public int getSword( ) {
                    return sword[0];
                }

                @Override
                public int getRod( ) {
                    return rod[0];
                }

                @Override
                public int getBow( ) {
                    return bow[0];
                }

                @Override
                public int getCompass( ) {
                    return compass[0];
                }

                @Override
                public int getFns( ) {
                    return fns[0];
                }
            } );
            player.sendMessage( plugin.getPrefix( ) + "§7Dein Inventar wurde erfolgreich §aabgespeichert§8." );
        } );
        //</editor-fold>
        //<editor-fold desc="InventoryClickEvent">
        plugin.getEventManager( ).registerListener( InventoryClickEvent.class, ( EventListener< InventoryClickEvent > ) ( InventoryClickEvent event ) -> {
            event.setCancelled( true );
            if ( event.getInventory( ) == null )
                return;
            org.bukkit.inventory.Inventory inventory = event.getInventory( );
            if ( inventory.getName( ).equals( "§9FFA §8➟ §7Inventarsotierung" ) ) {
                if ( inventory != event.getView( ).getTopInventory( ) )
                    return;
                if ( event.getRawSlot( ) > 9 )
                    return;
                event.setCancelled( false );
                return;
            }
        } );
        //</editor-fold>
    }
    //</editor-fold>

    //<editor-fold desc="openSortInventory">
    public void openSortInventory( Player player ) {
        org.bukkit.inventory.Inventory inventory = plugin.getServer( ).createInventory( null, 9, "§9FFA §8➟ §7Inventarsotierung" );
        player.openInventory( inventory );

        Inventory ffaInventory = plugin.getUserManager( ).getUser( player ).getInventory( );

        inventory.setItem( ffaInventory.getSword( ), sword.build( ) );
        inventory.setItem( ffaInventory.getRod( ), rod.build( ) );
        inventory.setItem( ffaInventory.getBow( ), bow.build( ) );
        inventory.setItem( ffaInventory.getCompass( ), compass.build( ) );
        inventory.setItem( ffaInventory.getFns( ), fns.build( ) );

        player.updateInventory( );
    }
    //</editor-fold>

    //<editor-fold desc="fillSpawnInventory">
    public void fillSpawnInventory( Player player ) {
        player.closeInventory( );
        player.getInventory( ).clear( );
        player.getInventory( ).setArmorContents( null );
        player.updateInventory( );

        player.getInventory( ).setItem( 0, inventoryItem.build( ) );
        player.getInventory( ).setItem( 8, leaveItem.build( ) );

        if ( !player.hasPermission( "ffa.command.spectator" ) )
            return;
        player.getInventory( ).setItem( 1, specItem.build( ) );
    }
    //</editor-fold>

    //<editor-fold desc="fillGameInventory">
    public void fillGameInventory( Player player ) {
        player.closeInventory( );
        player.getInventory( ).clear( );
        player.getInventory( ).setArmorContents( null );
        player.updateInventory( );

        Inventory inventory = plugin.getUserManager( ).getUser( player ).getInventory( );

        player.getInventory( ).setItem( inventory.getSword( ), sword.build( ) );
        player.getInventory( ).setItem( inventory.getRod( ), rod.build( ) );
        player.getInventory( ).setItem( inventory.getBow( ), bow.build( ) );
        player.getInventory( ).setItem( inventory.getCompass( ), compass.build( ) );

        player.getInventory( ).setHelmet( head.build( ) );
        player.getInventory( ).setChestplate( chest.build( ) );
        player.getInventory( ).setLeggings( leggigns.build( ) );
        player.getInventory( ).setBoots( boots.build( ) );

        addArrows( player, 16 );
        resetFns( player, 61 );
    }
    //</editor-fold>

    //<editor-fold desc="addArrows">
    public void addArrows( Player player, int amount ) {
        player.getInventory( ).addItem( new ItemBuilder( Material.ARROW, amount ).setDisplayName( "§7Pfeil" ).build( ) );
    }
    //</editor-fold>

    //<editor-fold desc="resetFns">
    public void resetFns( Player player, int data ) {
        Inventory inventory = plugin.getUserManager( ).getUser( player ).getInventory( );

        ItemBuilder fns = this.fns;
        fns.setData( data );

        player.getInventory( ).setItem( inventory.getFns( ), fns.build( ) );
    }
    //</editor-fold>

    //<editor-fold desc="getSpecItem">
    public ItemBuilder getSpecItem( ) {
        return specItem;
    }
    //</editor-fold>

    //<editor-fold desc="getSword">
    public ItemBuilder getSword( ) {
        return sword;
    }
    //</editor-fold>

    //<editor-fold desc="getBow">
    public ItemBuilder getBow( ) {
        return bow;
    }
    //</editor-fold>

    //<editor-fold desc="getHead">
    public ItemBuilder getHead( ) {
        return head;
    }
    //</editor-fold>

    //<editor-fold desc="getChest">
    public ItemBuilder getChest( ) {
        return chest;
    }
    //</editor-fold>

    //<editor-fold desc="getLeggigns">
    public ItemBuilder getLeggigns( ) {
        return leggigns;
    }
    //</editor-fold>

    //<editor-fold desc="getBoots">
    public ItemBuilder getBoots( ) {
        return boots;
    }
    //</editor-fold>

    //<editor-fold desc="sendToFallback">
    public void sendToFallback( Player player ) {
        plugin.getServer( ).getMessenger( ).registerOutgoingPluginChannel( plugin, "CloudNet" );
        ByteArrayDataOutput byteArrayDataOutput = ByteStreams.newDataOutput( );
        byteArrayDataOutput.writeUTF( "Fallback" );
        player.sendPluginMessage( plugin, "CloudNet", byteArrayDataOutput.toByteArray( ) );
    }
    //</editor-fold>
}

package de.m0xitz.ffa.user;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.event.KillstreakChangeEvent;
import de.m0xitz.ffa.misc.EventListener;
import de.m0xitz.ffa.user.inventory.Inventory;
import de.m0xitz.ffa.user.stats.Stats;
import de.ncrypted.vtab.manager.VTabAPI;
import de.ncrypted.vtab.manager.nick.NickManager;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class UserManager {

    private Main plugin;

    private HashMap< UUID, User > users;
    private List< User > usersList;

    public UserManager( Main plugin ) {
        this.plugin = plugin;

        users = Maps.newHashMap( );
        usersList = Lists.newArrayList( );
        init( );
    }

    //<editor-fold desc="init">
    private void init( ) {
        //<editor-fold desc="PlayerDisconnectEvent ">
        plugin.getEventManager( ).registerListener( PlayerJoinEvent.class, ( EventListener< PlayerJoinEvent > ) ( PlayerJoinEvent event ) -> {
            Player player = event.getPlayer( );
            registerUser( player );

            if ( !plugin.getStatsManager( ).isInCache( player.getUniqueId( ) ) )
                return;
            plugin.getStatsManager( ).removeFromCache( player.getUniqueId( ) );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerQuitEvent">
        plugin.getEventManager( ).registerListener( PlayerQuitEvent.class, ( EventListener< PlayerQuitEvent > ) ( PlayerQuitEvent event ) -> {
            Player player = event.getPlayer( );
            User user = users.get( player.getUniqueId( ) );

            for ( User allUser : plugin.getUserManager( ).getUsers( ) ) {
                if ( allUser.equals( user ) )
                    continue;
                if ( !allUser.hasLastDamager( ) )
                    continue;
                if ( !player.equals( allUser.getLastDamager( ) ) )
                    continue;
                allUser.setLastDamager( null );
            }

            unregisterUser( player );
        } );
        //</editor-fold>
        //<editor-fold desc="KillstreakChangeEvent">
        plugin.getEventManager( ).registerListener( KillstreakChangeEvent.class, ( EventListener< KillstreakChangeEvent > ) ( KillstreakChangeEvent event ) -> {
            Player player = event.getPlayer( );
            User user = users.get( player.getUniqueId( ) );
            long killstreak = event.getNewKillstreak( );
            long oldKillstreak = event.getOldKillstreak( );

            if ( event.getAction( ) == KillstreakChangeEvent.Action.ADD ) {
                if ( user.getStats( ).getHighestKillstreak( ) == oldKillstreak )
                    user.setHighestKillstreak( killstreak );
                String math = killstreak + "";
                player.sendMessage( math );
                if ( killstreak < 5 )
                    return;
                if ( !( math.substring( math.length( ) - 1, math.length( ) ).equals( "5" ) || math.substring( math.length( ) - 1, math.length( ) ).equals( "0" ) ) )
                    return;
                for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
                    all.sendMessage( plugin.getPrefix( ) + VTabAPI.getChatPrefix( player.getUniqueId( ) ) + NickManager.getName( player ) + " §bhat eine §e" + killstreak + " §bKillreihe erreicht§8." );
                    plugin.getPlayerManager( ).sendTitle( all, VTabAPI.getChatPrefix( player.getUniqueId( ) ) + NickManager.getName( player ), "hat eine §e" + killstreak + " §7Killreihe erreicht" );
                }
            }
            if ( oldKillstreak < 15 )
                return;
            for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
                all.sendMessage( plugin.getPrefix( ) + "§bDie Killreihe von " + VTabAPI.getChatPrefix( player.getUniqueId( ) ) + NickManager.getName( player ) + " §8[§7" + oldKillstreak + " §cKills§8] §bwurde beendet§8." );
            }
        } );
        //</editor-fold>
    }
    //</editor-fold>

    //<editor-fold desc="registerUser">
    public void registerUser( Player player ) {
        User User = new User( plugin, player );
        users.put( player.getUniqueId( ), User );
        usersList.add( User );

        plugin.getDatabaseManager( ).getDatabase( ).getInventory( player.getUniqueId( ), new Consumer< Inventory >( ) {
            @Override
            public void accept( Inventory inventory ) {
                User.setInventory( inventory );
            }
        } );
        plugin.getDatabaseManager( ).getDatabase( ).getStats( player.getUniqueId( ), new Consumer< Stats >( ) {
            @Override
            public void accept( Stats stats ) {
                User.setStats( stats );
            }
        } );
    }
    //</editor-fold>

    //<editor-fold desc="unregisterUser">
    public void unregisterUser( Player player ) {
        User User = getUser( player );

        plugin.getDatabaseManager( ).getDatabase( ).setInventory( player.getUniqueId( ), User.getInventory( ) );

        if ( plugin.getStatsManager( ).isInCache( player.getUniqueId( ) ) )
            return;
        plugin.getStatsManager( ).addToCache( player.getUniqueId( ), selectStats( player.getUniqueId( ) ) );

        users.remove( player.getUniqueId( ) );
        usersList.remove( User );
        plugin.getDatabaseManager( ).getDatabase( ).setStats( player.getUniqueId( ), User.getStats( ) );
    }
    //</editor-fold>

    //<editor-fold desc="getUser">
    public User getUser( Player player ) {
        return users.get( player.getUniqueId( ) );
    }

    public User getUser( UUID uuid ) {
        return users.get( uuid );
    }
    //</editor-fold>

    //<editor-fold desc="getUsers">
    public List< User > getUsers( ) {
        return usersList;
    }
    //</editor-fold>

    //<editor-fold desc="selectStats">
    public Stats selectStats( UUID uuid ) {
        if ( plugin.getServer( ).getPlayer( uuid ) != null ) {
            Stats stats;
            try {
                stats = getUser( plugin.getServer( ).getPlayer( uuid ) ).getStats( );
            } catch ( NullPointerException ex ) {
                stats = new Stats( ) {
                    @Override
                    public long getKills( ) {
                        return 0;
                    }

                    @Override
                    public long getDeaths( ) {
                        return 0;
                    }

                    @Override
                    public long getPoints( ) {
                        return 100;
                    }

                    @Override
                    public long getJoins( ) {
                        return 0;
                    }

                    @Override
                    public long getHighestKillstreak( ) {
                        return 0;
                    }
                };
            }
            return stats;
        }
        if ( plugin.getStatsManager( ).isInCache( uuid ) )
            return plugin.getStatsManager( ).getCacheStats( uuid );

        Stats[] stats = { new Stats( ) {
            @Override
            public long getKills( ) {
                return 0;
            }

            @Override
            public long getDeaths( ) {
                return 0;
            }

            @Override
            public long getPoints( ) {
                return 100;
            }

            @Override
            public long getJoins( ) {
                return 0;
            }

            @Override
            public long getHighestKillstreak( ) {
                return 0;
            }
        } };
        plugin.getDatabaseManager( ).getDatabase( ).getStats( uuid, stats1 -> {
            stats[0] = stats1;
        } );
        plugin.getStatsManager( ).addToCache( uuid, stats[0] );
        return stats[0];
    }
    //</editor-fold>
}

package de.m0xitz.ffa.misc;

import org.bukkit.event.Event;

public interface EventListener< T extends Event > {

    void on( T event );
}

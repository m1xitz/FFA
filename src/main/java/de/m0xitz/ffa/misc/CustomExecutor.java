package de.m0xitz.ffa.misc;

import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.Listener;
import org.bukkit.plugin.EventExecutor;

public class CustomExecutor implements EventExecutor {

    private Class< ? extends Event > eventClass;
    private EventListener eventListener;

    private boolean disable;

    public CustomExecutor( Class< ? extends Event > eventClass, EventListener eventListener ) {
        this.eventClass = eventClass;
        this.eventListener = eventListener;

        disable = false;
    }

    //<editor-fold desc="execute">
    @Override
    public void execute( Listener listener, Event event ) throws EventException {
        if ( disable ) {
            event.getHandlers( ).unregister( listener );
            return;
        }
        if ( !eventClass.equals( event.getClass( ) ) )
            return;
        eventListener.on( event );
    }
    //</editor-fold>

    //<editor-fold desc="setDisable">
    public void setDisable( boolean disable ) {
        this.disable = disable;
    }
    //</editor-fold>

    //<editor-fold desc="getEventListener">
    public EventListener getEventListener( ) {
        return eventListener;
    }
    //</editor-fold>
}

package de.m0xitz.ffa.misc;

import com.comphenix.packetwrapper.WrapperPlayServerScoreboardDisplayObjective;
import com.comphenix.packetwrapper.WrapperPlayServerScoreboardObjective;
import com.comphenix.packetwrapper.WrapperPlayServerScoreboardScore;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.google.common.collect.Maps;
import de.m0xitz.ffa.user.User;

import java.util.HashMap;

public class ScoreboardBuilder {

    private User user;
    private String title;
    private HashMap< Integer, String > lines;
    private ScoreboardAPI scoreboardAPI;

    public ScoreboardBuilder( User user, String title ) {
        this.user = user;
        this.title = title;
        this.lines = Maps.newHashMap( );
        this.scoreboardAPI = new ScoreboardAPI( user );
    }

    //<editor-fold desc="setLine">
    public void setLine( int line, String text ) {
        if ( lines.containsKey( line ) )
            scoreboardAPI.removeLine( line );
        lines.put( line, text );
        scoreboardAPI.setLine( line, text );
    }
    //</editor-fold>

    //<editor-fold desc="removeLine">
    public void removeLine( int line ) {
        if ( !lines.containsKey( line ) )
            return;
        scoreboardAPI.removeLine( line );
        lines.remove( line );
    }
    //</editor-fold>

    //<editor-fold desc="setup">
    public void setup( ) {
        this.scoreboardAPI.remove( );
        this.scoreboardAPI.sendSidebar( title );

        for ( int score : lines.keySet( ) ) {
            String line = lines.get( score );
            scoreboardAPI.setLine( score, line );
        }
    }
    //</editor-fold>

    //<editor-fold desc="setTitle">
    public void setTitle( String title ) {
        this.title = title;
        scoreboardAPI.setName( title );
    }
    //</editor-fold>

    //<editor-fold desc="remove">
    public void remove( ) {
        this.scoreboardAPI.remove( );
    }
    //</editor-fold>

    //<editor-fold desc="getUser">
    public User getUser( ) {
        return user;
    }
    //</editor-fold>

    //<editor-fold desc="ScoreboardAPI">
    public class ScoreboardAPI {

        private User user;
        private HashMap< Integer, String > lines;
        private boolean displayed;

        public ScoreboardAPI( User user ) {
            this.user = user;
            this.lines = new HashMap<>( );
            this.displayed = false;
        }

        //<editor-fold desc="sendSidebar">
        public void sendSidebar( String displayedName ) {
            if ( displayed )
                return;

            WrapperPlayServerScoreboardObjective packetScoreboard = new WrapperPlayServerScoreboardObjective( );
            packetScoreboard.setDisplayName( displayedName );
            packetScoreboard.setName( "sideboard" );
            packetScoreboard.setMode( 0 );
            packetScoreboard.setHealthDisplay( WrapperPlayServerScoreboardObjective.HealthDisplay.INTEGER );

            WrapperPlayServerScoreboardDisplayObjective packetPlayOut = new WrapperPlayServerScoreboardDisplayObjective( );
            packetPlayOut.setPosition( 1 );
            packetPlayOut.setScoreName( "sideboard" );

            user.sendPacket( packetScoreboard );
            user.sendPacket( packetPlayOut );

            displayed = true;
        }
        //</editor-fold>

        //<editor-fold desc="remove">
        public void remove( ) {
            WrapperPlayServerScoreboardObjective packetScoreboard = new WrapperPlayServerScoreboardObjective( );
            packetScoreboard.setDisplayName( "" );
            packetScoreboard.setName( "sideboard" );
            packetScoreboard.setMode( 1 );
            user.sendPacket( packetScoreboard );

            displayed = false;
        }
        //</editor-fold>

        //<editor-fold desc="setLine">
        public void setLine( Integer line, String text ) {
            if ( !displayed )
                return;
            if ( text.length( ) > 40 )
                text = text.substring( 0, 40 );

            WrapperPlayServerScoreboardScore packetScore = new WrapperPlayServerScoreboardScore( );
            packetScore.setObjectiveName( "sideboard" );
            packetScore.setScoreName( text );
            packetScore.setValue( line );
            packetScore.setScoreboardAction( EnumWrappers.ScoreboardAction.CHANGE );
            user.sendPacket( packetScore );

            this.lines.put( line, text );
        }
        //</editor-fold>

        //<editor-fold desc="removeLine">
        public void removeLine( Integer line ) {
            if ( !displayed )
                return;
            if ( !lines.containsKey( line ) )
                return;
            String text = lines.get( line );

            WrapperPlayServerScoreboardScore packetScore = new WrapperPlayServerScoreboardScore( );
            packetScore.setObjectiveName( "sideboard" );
            packetScore.setScoreName( text );
            packetScore.setValue( line );
            packetScore.setScoreboardAction( EnumWrappers.ScoreboardAction.REMOVE );
            user.sendPacket( packetScore );

            lines.remove( line );
        }
        //</editor-fold>

        //<editor-fold desc="setName">
        public void setName( String displayedName ) {
            if ( !displayed )
                return;
            WrapperPlayServerScoreboardObjective packetScoreboard = new WrapperPlayServerScoreboardObjective( );
            packetScoreboard.setDisplayName( displayedName );
            packetScoreboard.setName( "sideboard" );
            packetScoreboard.setMode( 2 );
            user.sendPacket( packetScoreboard );
        }
        //</editor-fold>

        //<editor-fold desc="getLine">
        public String getLine( Integer line ) {
            return lines.get( line );
        }
        //</editor-fold>

    }
    //</editor-fold>

}

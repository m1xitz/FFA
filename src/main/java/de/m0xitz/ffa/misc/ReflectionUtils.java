package de.m0xitz.ffa.misc;

import com.mojang.authlib.GameProfile;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionUtils {

    //<editor-fold desc="getTexture">
    public static String getTexture( Player player ) {
        try {
            String craftPath = Bukkit.getServer( ).getClass( ).getPackage( ).getName( );
            Object craftPlayer = Class.forName( String.valueOf( craftPath ) + ".entity.CraftPlayer" ).cast( ( Object ) player );
            Method getHandle = craftPlayer.getClass( ).getDeclaredMethod( "getHandle", new Class[0] );
            Object entityPlayer = getHandle.invoke( craftPlayer, new Object[0] );
            Method getProfile = entityPlayer.getClass( ).getMethod( "getProfile", new Class[0] );
            GameProfile profile = ( GameProfile ) getProfile.invoke( entityPlayer, new Object[0] );
            return profile.getProperties( ).get( "textures" ).iterator( ).next( ).getValue( );
        } catch ( Exception ex ) {
            ex.printStackTrace( );
            return null;
        }
    }
    //</editor-fold>

    //<editor-fold desc="getMcClass">
    public static Class< ? > getMcClass( String name ) throws ClassNotFoundException {
        String version = Bukkit.getServer( ).getClass( ).getPackage( ).getName( ).substring( 23 );
        return Class.forName( "net.minecraft.commands." + version + "." + name );
    }
    //</editor-fold>

    //<editor-fold desc="getCraftClass">
    public static Class< ? > getCraftClass( String name ) throws ClassNotFoundException {
        String version = Bukkit.getServer( ).getClass( ).getPackage( ).getName( );
        return Class.forName( String.valueOf( version ) + "." + name );
    }
    //</editor-fold>

    //<editor-fold desc="set">
    public static void set( Object obj, String name, Object value ) {
        try {
            Field field = obj.getClass( ).getDeclaredField( name );
            field.setAccessible( true );
            field.set( obj, value );
        } catch ( Exception e ) {
            e.printStackTrace( );
        }
    }
    //</editor-fold>

    //<editor-fold desc="get">
    public static Object get( Object obj, String name ) {
        try {
            Field field = obj.getClass( ).getDeclaredField( name );
            field.setAccessible( true );
            return field.get( obj );
        } catch ( Exception e ) {
            e.printStackTrace( );
            return null;
        }
    }

    public static Object get( Object obj, Class clazz, String name ) {
        try {
            Field field = clazz.getDeclaredField( name );
            field.setAccessible( true );
            return field.get( obj );
        } catch ( Exception e ) {
            e.printStackTrace( );
            return null;
        }
    }
    //</editor-fold>
}

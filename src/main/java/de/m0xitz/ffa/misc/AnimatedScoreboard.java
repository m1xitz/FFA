package de.m0xitz.ffa.misc;

import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.user.User;
import org.bukkit.scheduler.BukkitTask;

public class AnimatedScoreboard implements Runnable {

    private Main plugin;

    private BukkitTask countId;
    private String state;

    public AnimatedScoreboard( Main plugin ) {
        this.plugin = plugin;

        countId = plugin.runTaskTimer( 0, 10 * 60, this );
        state = "§8» §bVentage§8.§beu";
    }

    @Override
    public void run( ) {
        startAnimation( );
    }

    //<editor-fold desc="startAnimation">
    private void startAnimation( ) {
        for ( User user : plugin.getUserManager( ).getUsers( ) ) {
            state = "§8» §bVentage§8.§beu";
            user.getScoreboard( ).setTitle( state );
        }
        plugin.runTaskLaterAsync( 10, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bentage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 2, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bntage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 3, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §btage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 4, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 5, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bge§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 6, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §be§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 7, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 8, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 9, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 10, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §b";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 11, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §9A";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 12, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §9FA";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 13, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §9FFA";
                user.getScoreboard( ).setTitle( state );
            }
        } );


        plugin.runTaskLaterAsync( 10 * 30, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §9FA";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 31, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §9A";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 32, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §b";
                user.getScoreboard( ).setTitle( state );
            }
        } );

        plugin.runTaskLaterAsync( 10 * 33, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 34, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 35, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 36, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §be§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 37, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bge§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 38, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 39, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §btage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 40, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bntage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 41, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bentage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
        plugin.runTaskLaterAsync( 10 * 42, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                state = "§8» §bVentage§8.§beu";
                user.getScoreboard( ).setTitle( state );
            }
        } );
    }
    //</editor-fold>

    //<editor-fold desc="getState">
    public String getState( ) {
        return state;
    }
    //</editor-fold>
}

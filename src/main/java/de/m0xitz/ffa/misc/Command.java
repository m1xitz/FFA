package de.m0xitz.ffa.misc;

import com.google.common.collect.Lists;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

public abstract class Command extends BukkitCommand {

    private boolean console;

    protected Command( String name ) {
        this( name, false );
    }

    protected Command( String name, boolean console ) {
        this( name, console, new String[0] );
    }

    protected Command( String name, String... aliases ) {
        super( name, "", "", Lists.newArrayList( aliases ) );
        this.console = false;
    }

    protected Command( String name, boolean console, String... aliases ) {
        super( name, "", "", Lists.newArrayList( aliases ) );
        this.console = console;
    }

    //<editor-fold desc="execute">
    @Override
    public boolean execute( CommandSender commandSender, String label, String[] strings ) {
        if ( !( commandSender instanceof Player ) ) {
            if ( console ) {
                onConsole( commandSender, strings );
                return true;
            }
            commandSender.sendMessage( "§cDieser Befehl ist nur für Spieler!" );
            return true;
        }
        Player player = ( Player ) commandSender;
        onPlayer( player, strings );
        return true;
    }
    //</editor-fold>

    //<editor-fold desc="onPlayer">
    public abstract void onPlayer( Player player, String[] strings );
    //</editor-fold>

    //<editor-fold desc="onConsole">
    public void onConsole( CommandSender commandSender, String[] strings ) {

    }
    //</editor-fold>
}

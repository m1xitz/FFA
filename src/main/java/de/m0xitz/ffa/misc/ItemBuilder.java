package de.m0xitz.ffa.misc;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class ItemBuilder {

    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private List< String > lore;
    private byte dyecolor;
    private byte potionid;
    private byte dataid;
    private LeatherArmorMeta armorMeta;
    private BookMeta bookMeta;
    private SkullMeta skullMeta;

    public ItemBuilder( Material material, int amount, short data ) {
        itemStack = new ItemStack( material, amount, data );
        itemMeta = itemStack.getItemMeta( );
        lore = Lists.newArrayList( );
        dyecolor = 20;
        potionid = 0;
        dataid = 0;
        if ( isLeather( ) ) {
            armorMeta = ( LeatherArmorMeta ) itemStack.getItemMeta( );
            return;
        }
        if ( material == Material.SKULL_ITEM ) {
            skullMeta = ( SkullMeta ) itemStack.getItemMeta( );
            return;
        }
        if ( material == Material.WRITTEN_BOOK )
            bookMeta = ( BookMeta ) itemStack.getItemMeta( );
    }

    public ItemBuilder( Material material, int amount ) {
        this( material, amount, ( short ) 0 );
    }

    public ItemBuilder( Material material ) {
        this( material, 1 );
    }

    //<editor-fold desc="setDisplayName">
    public ItemBuilder setDisplayName( String displayName ) {
        itemMeta.setDisplayName( displayName );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setUnbreakable">
    public ItemBuilder setUnbreakable( boolean unbreakable ) {
        itemMeta.spigot( ).setUnbreakable( unbreakable );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setOwner">
    public ItemBuilder setOwner( String owner ) {
        itemStack.setDurability( ( short ) 3 );

        skullMeta.setOwner( owner );
        return this;
    }

    public ItemBuilder setOwner( Property property ) {
        itemStack.setDurability( ( short ) 3 );

        GameProfile gameProfile = new GameProfile( UUID.randomUUID( ), null );
        gameProfile.getProperties( ).put( "textures", property );

        try {
            Field field = skullMeta.getClass( ).getDeclaredField( "profile" );
            field.setAccessible( true );
            field.set( skullMeta, gameProfile );
            field.setAccessible( false );
        } catch ( NoSuchFieldException | IllegalAccessException ex ) {
            ex.printStackTrace( );
        }
        return this;
    }

    public ItemBuilder setOwnerUrl( String url ) {
        byte[] encodedData = Base64.getEncoder( ).encode( String.format( "{\"textures\":{\"SKIN\":{\"url\":\"%s\"}}}", url ).getBytes( ) );
        setOwner( new Property( "textures", new String( encodedData ) + "=" ) );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setLore">
    public ItemBuilder setLore( String... lore ) {
        Collections.addAll( this.lore, lore );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setLeatherColor">
    public ItemBuilder setLeatherColor( Color color ) {
        if ( !isLeather( ) )
            return this;
        armorMeta.setColor( color );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setBookTitle">
    public ItemBuilder setBookTitle( String title ) {
        if ( bookMeta == null )
            return this;
        bookMeta.setTitle( title );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setBookAuthor">
    public ItemBuilder setBookAuthor( String author ) {
        if ( bookMeta == null )
            return this;
        bookMeta.setAuthor( author );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setBookPages">
    public ItemBuilder setBookPages( String... content ) {
        if ( bookMeta == null )
            return this;
        bookMeta.setPages( content );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setColor">
    public ItemBuilder setColor( int colorid ) {
        dyecolor = ( byte ) colorid;
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setData">
    public ItemBuilder setData( int data ) {
        dataid = ( byte ) data;
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="setPotion">
    public ItemBuilder setPotion( int potion ) {
        potionid = ( byte ) potion;
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="addEnchantment">
    public ItemBuilder addEnchantment( Enchantment enchantment, int level ) {
        itemMeta.addEnchant( enchantment, level, true );
        return this;
    }
    //</editor-fold>

    //<editor-fold desc="isLeather">
    private boolean isLeather( ) {
        return itemStack.getType( ) == Material.LEATHER_BOOTS || itemStack.getType( ) == Material.LEATHER_CHESTPLATE ||
                itemStack.getType( ) == Material.LEATHER_HELMET || itemStack.getType( ) == Material.LEATHER_LEGGINGS;
    }
    //</editor-fold>

    //<editor-fold desc="getDisplayName">
    private String getDisplayName( ) {
        return this.itemMeta.getDisplayName( );
    }
    //</editor-fold>

    //<editor-fold desc="getAmount">
    private int getAmount( ) {
        return this.itemStack.getAmount( );
    }
    //</editor-fold>

    //<editor-fold desc="getMaterial">
    private Material getMaterial( ) {
        return this.itemStack.getType( );
    }
    //</editor-fold>

    //<editor-fold desc="isUnbreakable">
    private boolean isUnbreakable( ) {
        return this.itemMeta.spigot( ).isUnbreakable( );
    }
    //</editor-fold>

    //<editor-fold desc="getOwner">
    private String getOwner( ) {
        return this.skullMeta.getOwner( );
    }
    //</editor-fold>

    //<editor-fold desc="getLore">
    private List< String > getLore( ) {
        return this.lore;
    }
    //</editor-fold>

    //<editor-fold desc="getColor">
    private byte getColor( ) {
        return this.dyecolor;
    }
    //</editor-fold>

    //<editor-fold desc="getData">
    private byte getData( ) {
        return this.dataid;
    }
    //</editor-fold>

    //<editor-fold desc="getPotion">
    private byte getPotion( ) {
        return this.potionid;
    }
    //</editor-fold>

    //<editor-fold desc="build">
    public ItemStack build( ) {
        if ( dyecolor < 20 )
            itemStack = new ItemStack( itemStack.getType( ), itemStack.getAmount( ), dyecolor );
        if ( potionid > 0 )
            itemStack = new ItemStack( itemStack.getType( ), itemStack.getAmount( ), potionid );
        if ( dataid != 0 )
            itemStack = new ItemStack( itemStack.getType( ), itemStack.getAmount( ), dataid );

        if ( skullMeta != null ) {
            skullMeta.setDisplayName( getDisplayName( ) );
            skullMeta.spigot( ).setUnbreakable( isUnbreakable( ) );
            skullMeta.setLore( lore );
            itemStack.setItemMeta( skullMeta );
            return itemStack;
        }
        if ( armorMeta != null ) {
            armorMeta.setDisplayName( getDisplayName( ) );
            armorMeta.spigot( ).setUnbreakable( isUnbreakable( ) );
            armorMeta.setLore( lore );
            itemStack.setItemMeta( armorMeta );
            return itemStack;
        }
        if ( bookMeta != null ) {
            bookMeta.setDisplayName( getDisplayName( ) );
            bookMeta.spigot( ).setUnbreakable( isUnbreakable( ) );
            bookMeta.setLore( lore );
            itemStack.setItemMeta( bookMeta );
            return itemStack;
        }
        itemMeta.setLore( lore );
        itemStack.setItemMeta( itemMeta );
        return itemStack;
    }
    //</editor-fold>
}

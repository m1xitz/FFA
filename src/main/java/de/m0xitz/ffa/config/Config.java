package de.m0xitz.ffa.config;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Config {

    private String name;

    private File file;
    private FileConfiguration fileConfiguration;

    private LoadingCache< String, Object > cache;

    public Config( String name ) {
        this.name = name;

        file = new File( "plugins/FFA", name + ".yml" );
        fileConfiguration = YamlConfiguration.loadConfiguration( file );
        fileConfiguration.options( ).copyDefaults( true );

        cache = CacheBuilder.newBuilder( ).maximumSize( 100 ).expireAfterAccess( 30, TimeUnit.MINUTES ).build( new CacheLoader< String, Object >( ) {
            @Override
            public Object load( String path ) throws Exception {
                return fileConfiguration.get( path );
            }
        } );
    }

    //<editor-fold desc="get">
    public Object get( String path ) {
        try {
            return cache.get( path );
        } catch ( ExecutionException e ) {
            return null;
        }
    }
    //</editor-fold>    (

    //<editor-fold desc="set">
    public void set( String path, Object value ) {
        fileConfiguration.set( path, value );
    }
    //</editor-fold>

    //<editor-fold desc="contains">
    public boolean contains( String path ) {
        return fileConfiguration.contains( path );
    }
    //</editor-fold>

    //<editor-fold desc="addDefaults">
    public void addDefaults( String path, Object value ) {
        fileConfiguration.addDefault( path, value );
    }
    //</editor-fold>

    //<editor-fold desc="getString">
    public String getString( String path ) {
        return ( String ) get( path );
    }
    //</editor-fold>

    //<editor-fold desc="getInt">
    public int getInt( String path ) {
        return ( int ) get( path );
    }
    //</editor-fold>

    //<editor-fold desc="getLocation">
    public Location getLocation( String path ) {
        return ( Location ) get( path );
    }
    //</editor-fold>

    //<editor-fold desc="save">
    public void save( ) {
        try {
            fileConfiguration.save( file );
        } catch ( IOException ex ) {
            ex.printStackTrace( );
        }
    }

    //</editor-fold>

    //<editor-fold desc="reload">
    public void reload( ) {
        save( );
        cache.cleanUp( );
    }
    //</editor-fold>

    //<editor-fold desc="getName">
    public String getName( ) {
        return name;
    }
    //</editor-fold>

    //<editor-fold desc="getFileConfiguration">
    public FileConfiguration getFileConfiguration( ) {
        return fileConfiguration;
    }
    //</editor-fold>

}

package de.m0xitz.ffa.config;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import de.m0xitz.ffa.Main;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class ConfigManager {

    private Main plugin;

    private LoadingCache< String, Config > cache;

    public ConfigManager( Main plugin ) {
        this.plugin = plugin;

        cache = CacheBuilder.newBuilder( ).maximumSize( 100 ).expireAfterAccess( 30, TimeUnit.MINUTES ).build( new CacheLoader< String, Config >( ) {
            @Override
            public Config load( String name ) throws Exception {
                return new Config( name );
            }
        } );
    }

    //<editor-fold desc="getConfig">
    public Config getConfig( String name ) {
        try {
            return cache.get( name );
        } catch ( ExecutionException e ) {
            return null;
        }
    }
    //</editor-fold>

}

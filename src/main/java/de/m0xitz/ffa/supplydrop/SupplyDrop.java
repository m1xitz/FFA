package de.m0xitz.ffa.supplydrop;

import com.comphenix.packetwrapper.WrapperPlayServerGameStateChange;
import com.comphenix.packetwrapper.WrapperPlayServerWorldParticles;
import com.comphenix.protocol.wrappers.EnumWrappers;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.ItemBuilder;
import de.m0xitz.ffa.user.User;
import de.slikey.effectlib.util.ParticleEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

public class SupplyDrop implements Runnable {

    private Main plugin;

    private Random random;
    private Location location;
    private List<Location> locations;

    private int count;
    private BukkitTask countId;
    private BukkitTask soundId;
    private int effectCount;
    private BukkitTask effectId;

    private boolean exists;

    public SupplyDrop( Main plugin ) {
        this.plugin = plugin;

        random = new Random( );
        locations = plugin.getLocationManager( ).getSupplyDropLocations( );
        location = locations.get( random.nextInt( locations.size( ) - 1 ) ).clone( );
        count = pickRandom( 60, 300 );

        countId = plugin.runTaskTimer( 0, 20, this );
    }

    //<editor-fold desc="run">
    @Override
    public void run( ) {
        for ( User user : plugin.getUserManager( ).getUsers( ) ) {
            user.setLine( 1, "§8➟ §d" + getCount( ) );
        }

        if ( count < 6 && count > 0 ) {
            for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
                all.sendMessage( plugin.getPrefix( ) + "§7Der SupplyDrop spawnt in §e" + count + " §7" + ( count == 1 ? "Sekunde" : "Sekunden" ) + "§8." );
                all.playSound( all.getLocation( ), Sound.NOTE_PLING, 1, 1 );
            }
        }

        if ( count == 0 ) {
            spawn( );
            for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
                all.sendMessage( plugin.getPrefix( ) + "§7Der SupplyDrop ist gespawnt§8." );
                all.playSound( all.getLocation( ), Sound.NOTE_PLING, 16, 16 );
            }
            plugin.getServer( ).getScheduler( ).cancelTask( countId.getTaskId( ) );
            return;
        }
        count--;
    }
    //</editor-fold>

    //<editor-fold desc="spawn">
    public void spawn( ) {
        for ( User user : plugin.getUserManager( ).getUsers( ) ) {
            user.setLine( 1, "§8➟ §dGespawnt §a✔" );
            user.getPlayer( ).setCompassTarget( location );
        }

        exists = true;

        Block block = location.getBlock( );
        block.setType( Material.CHEST );

        //<editor-fold desc="SoundCount">
        plugin.setMetadata( block, "supplyDrop", true );
        soundId = plugin.runTaskTimer( 0, 20, ( ) -> {
            if ( !exists )
                plugin.getServer( ).getScheduler( ).cancelTask( soundId.getTaskId( ) );
            location.getWorld( ).playSound( location, Sound.FIREWORK_LAUNCH, 1, 1 );

        } );
        //</editor-fold>
        //<editor-fold desc="EffectCount">
        effectCount = 15;
        Location location = this.location.clone( ).add( 0.5, 0.2, 0.5 );
        effectId = plugin.runTaskTimer( 0, 2, ( ) -> {
            if ( !exists )
                plugin.getServer( ).getScheduler( ).cancelTask( effectId.getTaskId( ) );
            if ( effectCount == 15 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.8, 0.0, 0.0 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.8, 0.0, 0.0 ), 100.0 );
            }
            if ( effectCount == 14 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.75, 0.0, 0.43 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.75, 0.0, -0.43 ), 100.0 );
            }
            if ( effectCount == 13 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.65, 0.0, 0.65 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.65, 0.0, -0.65 ), 100.0 );
            }
            if ( effectCount == 12 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.43, 0.0, 0.75 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.43, 0.0, -0.75 ), 100.0 );
            }
            if ( effectCount == 11 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.0, 0.0, 0.8 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.0, 0.0, -0.8 ), 100.0 );
            }
            if ( effectCount == 10 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.43, 0.0, 0.75 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.43, 0.0, -0.75 ), 100.0 );
            }
            if ( effectCount == 9 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.65, 0.0, 0.65 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.65, 0.0, -0.65 ), 100.0 );
            }
            if ( effectCount == 8 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.75, 0.0, 0.43 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.75, 0.0, -0.43 ), 100.0 );
            }
            if ( effectCount == 7 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.8, 0.0, 0.0 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.8, 0.0, 0.0 ), 100.0 );
            }
            if ( effectCount == 6 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.75, 0.0, -0.43 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.75, 0.0, 0.43 ), 100.0 );
            }
            if ( effectCount == 5 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.65, 0.0, -0.65 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.65, 0.0, 0.65 ), 100.0 );
            }
            if ( effectCount == 4 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.43, 0.0, -0.75 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.43, 0.0, 0.75 ), 100.0 );
            }
            if ( effectCount == 3 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.0, 0.0, -0.8 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.0, 0.0, 0.8 ), 100.0 );
            }
            if ( effectCount == 2 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.43, 0.0, -0.75 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.43, 0.0, 0.75 ), 100.0 );
            }
            if ( effectCount == 1 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.65, 0.0, -0.65 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.65, 0.0, 0.65 ), 100.0 );
            }
            if ( effectCount == 0 ) {
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( 0.75, 0.0, -0.43 ), 100.0 );
                ParticleEffect.SPELL_WITCH.display( 0.0f, 0.0f, 0.0f, 0.0f, 5, location.clone( ).add( -0.75, 0.0, 0.43 ), 100.0 );
                effectCount = 15;
                return;
            }
            effectCount--;
        } );
        //</editor-fold>
    }
    //</editor-fold>

    //<editor-fold desc="despawn">
    public void despawn( ) {
        for ( User user : plugin.getUserManager( ).getUsers( ) ) {
            user.getPlayer( ).setCompassTarget( user.getPlayer( ).getWorld( ).getSpawnLocation( ) );
        }

        location.getBlock( ).setType( Material.AIR );
        location.getWorld( ).strikeLightningEffect( location );

        exists = false;

        location = locations.get( random.nextInt( locations.size( ) ) );
        count = pickRandom( 60, 300 );
        countId = plugin.runTaskTimer( 0, 20, this );
    }
    //</editor-fold>

    //<editor-fold desc="getSupply">
    public void getSupply( Player player ) {
        switch ( random.nextInt( 16 ) ) {
            //<editor-fold desc="case 0">
            case 0:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "Diamantenschwert" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                ItemBuilder sword = new ItemBuilder( Material.DIAMOND_SWORD );
                sword.setDisplayName( "§bSchwert" );
                sword.setUnbreakable( true );
                int slotSword = plugin.getUserManager( ).getUser( player ).getInventory( ).getSword( );
                player.getInventory( ).setItem( slotSword, sword.build( ) );

                plugin.runTaskLater( 20 * 10, ( ) -> {
                    if ( plugin.getLocationManager( ).onSpawn( player ) )
                        return;
                    player.getInventory( ).setItem( slotSword, plugin.getInventoryManager( ).getSword( ).build( ) );
                } );
                break;
            //</editor-fold>
            //<editor-fold desc="case 1">
            case 1:
                plugin.getPlayerManager( ).sendTitle( player, "§cPech", "Langsamkeit" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_NO, 1, 1 );

                player.addPotionEffect( new PotionEffect( PotionEffectType.SLOW, 20 * 10, 0 ) );
                break;
            //</editor-fold>
            //<editor-fold desc="case 2">
            case 2:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "+3 Pfeile" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                plugin.getInventoryManager( ).addArrows( player, 3 );
                break;
            //</editor-fold>
            //<editor-fold desc="case 3">
            case 3:
                plugin.getPlayerManager( ).sendTitle( player, "§cPech", "Feuer" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_NO, 1, 1 );

                player.setFireTicks( 20 * 5 );
                break;
            //</editor-fold>
            //<editor-fold desc="case 4">
            case 4:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "Power 2" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                ItemBuilder bow = new ItemBuilder( Material.BOW );
                bow.setDisplayName( "§bBogen" );
                bow.addEnchantment( Enchantment.ARROW_DAMAGE, 2 );
                bow.setUnbreakable( true );
                int slotBow = plugin.getUserManager( ).getUser( player ).getInventory( ).getBow( );
                player.getInventory( ).setItem( slotBow, bow.build( ) );

                plugin.runTaskLater( 20 * 10, ( ) -> {
                    if ( plugin.getLocationManager( ).onSpawn( player ) )
                        return;
                    player.getInventory( ).setItem( slotBow, plugin.getInventoryManager( ).getBow( ).build( ) );
                } );
                break;
            //</editor-fold>
            //<editor-fold desc="case 5">
            case 5:
                plugin.getPlayerManager( ).sendTitle( player, "§cPech", "Gift" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_NO, 1, 1 );

                player.addPotionEffect( new PotionEffect( PotionEffectType.POISON, 20 * 10, 0 ) );
                break;
            //</editor-fold>
            //<editor-fold desc="case 6">
            case 6:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "Schnelligkeit" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                player.addPotionEffect( new PotionEffect( PotionEffectType.SPEED, 20 * 10, 0 ) );
                break;
            //</editor-fold>
            //<editor-fold desc="case 7">
            case 7:
                plugin.getPlayerManager( ).sendTitle( player, "§cPech", "Schwäche" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_NO, 1, 1 );

                player.addPotionEffect( new PotionEffect( PotionEffectType.WEAKNESS, 20 * 10, 0 ) );
                break;
            //</editor-fold>
            //<editor-fold desc="case 8">
            case 8:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "Stärke" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                player.addPotionEffect( new PotionEffect( PotionEffectType.INCREASE_DAMAGE, 20 * 10, 0 ) );
                break;
            //</editor-fold>
            //<editor-fold desc="case 9">
            case 9:
                plugin.getPlayerManager( ).sendTitle( player, "§cPech", "Keine Brustpanzer" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_NO, 1, 1 );
                player.getInventory( ).setChestplate( new ItemStack( Material.AIR ) );

                plugin.runTaskLater( 20 * 10, ( ) -> {
                    if ( plugin.getLocationManager( ).onSpawn( player ) )
                        return;
                    player.getInventory( ).setChestplate( plugin.getInventoryManager( ).getChest( ).build( ) );
                } );
                break;
            //</editor-fold>
            //<editor-fold desc="case 10">
            case 10:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "Schutz 3" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                ItemBuilder head = new ItemBuilder( Material.IRON_HELMET );
                head.setDisplayName( "§bHelm" );
                head.addEnchantment( Enchantment.PROTECTION_ENVIRONMENTAL, 3 );
                head.setUnbreakable( true );

                ItemBuilder chest = new ItemBuilder( Material.IRON_CHESTPLATE );
                chest.setDisplayName( "§bBurstplatte" );
                chest.addEnchantment( Enchantment.PROTECTION_ENVIRONMENTAL, 3 );
                chest.setUnbreakable( true );

                ItemBuilder leggings = new ItemBuilder( Material.IRON_LEGGINGS );
                leggings.setDisplayName( "§bHose" );
                leggings.addEnchantment( Enchantment.PROTECTION_ENVIRONMENTAL, 3 );
                leggings.setUnbreakable( true );

                ItemBuilder boots = new ItemBuilder( Material.IRON_BOOTS );
                boots.setDisplayName( "§bSchuhe" );
                boots.addEnchantment( Enchantment.PROTECTION_ENVIRONMENTAL, 3 );
                boots.setUnbreakable( true );

                player.getInventory( ).setHelmet( head.build( ) );
                player.getInventory( ).setChestplate( chest.build( ) );
                player.getInventory( ).setLeggings( leggings.build( ) );
                player.getInventory( ).setBoots( boots.build( ) );

                plugin.runTaskLater( 20 * 10, ( ) -> {
                    if ( plugin.getLocationManager( ).onSpawn( player ) )
                        return;

                    player.getInventory( ).setHelmet( plugin.getInventoryManager( ).getHead( ).build( ) );
                    player.getInventory( ).setChestplate( plugin.getInventoryManager( ).getChest( ).build( ) );
                    player.getInventory( ).setLeggings( plugin.getInventoryManager( ).getLeggigns( ).build( ) );
                    player.getInventory( ).setBoots( plugin.getInventoryManager( ).getBoots( ).build( ) );

                } );
                break;
            //</editor-fold>
            //<editor-fold desc="case 11">
            case 11:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "+3 TNT" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                ItemBuilder tnt = new ItemBuilder( Material.TNT, 3 );
                tnt.setDisplayName( "§cInstant TNT" );

                player.getInventory( ).addItem( tnt.build( ) );
                break;
            //</editor-fold>
            //<editor-fold desc="case 12">
            case 12:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "Neues Feuerzeug" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );
                plugin.getInventoryManager( ).resetFns( player, 0 );
                break;
            //</editor-fold>
            //<editor-fold desc="case 13">
            case 13:
                plugin.getPlayerManager( ).sendTitle( player, "§cPech", "Demo" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_NO, 1, 1 );
                WrapperPlayServerGameStateChange gameStateChangePacket = new WrapperPlayServerGameStateChange( );
                gameStateChangePacket.setReason( 5 );
                gameStateChangePacket.setValue( 0 );

                plugin.getUserManager( ).getUser( player ).sendPacket( gameStateChangePacket );
                break;
            //</editor-fold>
            //<editor-fold desc="case 14">
            case 14:
                plugin.getPlayerManager( ).sendTitle( player, "§aGlück", "Jetpack" );
                player.playSound( player.getLocation( ), Sound.VILLAGER_YES, 1, 1 );

                ItemBuilder jetpack = new ItemBuilder( Material.BLAZE_POWDER );
                jetpack.setDisplayName( "§eJetpack §73x" );
                player.getInventory( ).addItem( jetpack.build( ) );
                break;
            //</editor-fold>
            //<editor-fold desc="case 15">
            case 15:
                player.setPlayerTime( 20000, true );

                plugin.getPlayerManager( ).sendTitle( player, "§cPech", "Schreck" );

                WrapperPlayServerWorldParticles worldParticlesPacket = new WrapperPlayServerWorldParticles( );
                worldParticlesPacket.setParticleType( EnumWrappers.Particle.MOB_APPEARANCE );
                worldParticlesPacket.setLongDistance( false );
                worldParticlesPacket.setX( 0 );
                worldParticlesPacket.setY( 0 );
                worldParticlesPacket.setZ( 0 );
                worldParticlesPacket.setOffsetX( 0 );
                worldParticlesPacket.setOffsetY( 0 );
                worldParticlesPacket.setOffsetZ( 0 );
                worldParticlesPacket.setNumberOfParticles( 1 );
                plugin.getUserManager( ).getUser( player ).sendPacket( worldParticlesPacket );

                player.playSound( player.getLocation( ), Sound.ENDERDRAGON_DEATH, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );
                player.playSound( player.getLocation( ), Sound.GHAST_SCREAM, 100, 1 );

                plugin.runTaskLater( 5, player::resetPlayerTime );
                break;
            //</editor-fold>

            //<editor-fold desc="default">
            default:
                plugin.getPlayerManager( ).sendTitle( player, "", "§cNiete" );
                player.playSound( player.getLocation( ), Sound.ANVIL_BREAK, 1, 1 );
                break;
            //</editor-fold>
        }
    }
    //</editor-fold>

    //<editor-fold desc="getCount">
    public String getCount( ) {
        double scale60 = 1.0 / 60;
        int mm = ( int ) ( count * scale60 );
        int ss = count - mm * 60;

        DecimalFormat format = new DecimalFormat( "00" );
        return format.format( mm ) + ":" + format.format( ss );
    }
    //</editor-fold>

    //<editor-fold desc="getLocation">
    public Location getLocation( ) {
        if ( !exists )
            return plugin.getLocationManager( ).getSpawnLocation( );
        return location;
    }
    //</editor-fold>

    //<editor-fold desc="exists">
    public boolean exists( ) {
        return exists;
    }
    //</editor-fold>

    //<editor-fold desc="pickRandom">
    private int pickRandom( int min, int max ) {
        return random.nextInt( max - min + 1 ) + min;
    }
    //</editor-fold>
}

package de.m0xitz.ffa.supplydrop;

import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.EventListener;
import de.slikey.effectlib.effect.SkyRocketEffect;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

public class SupplyDropManager {

    private Main plugin;

    private SupplyDrop supplyDrop;

    public SupplyDropManager( Main plugin ) {
        this.plugin = plugin;

        supplyDrop = new SupplyDrop( plugin );
        init( );
    }

    private void init( ) {
        //<editor-fold desc="PlayerInteractEvent">
        plugin.getEventManager( ).registerListener( PlayerInteractEvent.class, ( EventListener< PlayerInteractEvent > ) ( PlayerInteractEvent event ) -> {
            if ( event.getPlayer( ) == null )
                return;
            Player player = event.getPlayer( );
            if ( event.getAction( ) == null )
                return;
            Action action = event.getAction( );
            if ( action != Action.RIGHT_CLICK_BLOCK )
                return;
            if ( event.getClickedBlock( ) == null )
                return;
            Block block = event.getClickedBlock( );
            if ( !plugin.hasMetadata( block, "supplyDrop" ) )
                return;
            supplyDrop.getSupply( player );
            supplyDrop.despawn( );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerInteractEvent - Jetpack">
        plugin.getEventManager( ).registerListener( PlayerInteractEvent.class, ( EventListener< PlayerInteractEvent > ) ( PlayerInteractEvent event ) -> {
            if ( !plugin.getPlayerManager( ).checkPlayerInteractEvent( event ) )
                return;
            Action action = event.getAction( );
            if ( !( action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR ) )
                return;
            Player player = event.getPlayer( );
            ItemStack itemStack = player.getItemInHand( );
            ItemMeta itemMeta = itemStack.getItemMeta( );
            if ( !itemMeta.getDisplayName( ).startsWith( "§eJetpack" ) )
                return;
            event.setCancelled( true );
            int amount = Integer.parseInt( itemMeta.getDisplayName( ).replace( "§eJetpack §7", "" ).replace( "x", "" ) ) - 1;
            itemMeta.setDisplayName( "§eJetpack §7" + amount + "x" );
            itemStack.setItemMeta( itemMeta );

            Vector vector = player.getLocation( ).getDirection( ).normalize( ).multiply( 1.5 ).setY( 2.4 );
            player.setVelocity( vector );
            player.playSound( player.getLocation( ), Sound.WITHER_SHOOT, 16, 16 );

            SkyRocketEffect effect = new SkyRocketEffect( plugin.getEffectManager( ) );
            effect.setEntity( player );
            effect.setLocation( player.getLocation( ) );
            effect.start( );

            if ( amount != 0 )
                return;
            player.getInventory( ).remove( itemStack );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerJoinEvent">
        plugin.getEventManager( ).registerListener( PlayerJoinEvent.class, ( EventListener< PlayerJoinEvent > ) ( PlayerJoinEvent event ) -> {
            event.getPlayer( ).setCompassTarget( event.getPlayer( ).getWorld( ).getSpawnLocation( ) );
            if ( !supplyDrop.exists( ) )
                return;
            event.getPlayer( ).setCompassTarget( supplyDrop.getLocation( ) );
        } );
        //</editor-fold>
    }

    //<editor-fold desc="getSupplyDrop">
    public SupplyDrop getSupplyDrop( ) {
        return supplyDrop;
    }
    //</editor-fold>
}

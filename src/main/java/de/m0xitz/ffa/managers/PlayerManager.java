package de.m0xitz.ffa.managers;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.common.collect.Lists;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.EventListener;
import de.m0xitz.ffa.user.User;
import de.m0xitz.ffa.user.stats.Stats;
import de.ncrypted.coinapi.CoinApi;
import de.ncrypted.vtab.manager.VTabAPI;
import de.ncrypted.vtab.manager.nick.NickManager;
import net.minecraft.server.v1_8_R3.Packet;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.UUID;

public class PlayerManager {

    private Main plugin;

    private List< UUID > playerOnWater;

    public PlayerManager( Main plugin ) {
        this.plugin = plugin;

        playerOnWater = Lists.newArrayList( );
        init( );
    }

    //<editor-fold desc="init">
    private void init( ) {
        plugin.runTaskTimer( 0, 20, ( ) -> {
            for ( User user : plugin.getUserManager( ).getUsers( ) ) {
                if ( plugin.getSpectatorManager( ).isSpectator( user.getPlayer( ) ) ) {
                    user.sendActionbar( "§7Spectator" );
                    continue;
                }
                user.sendActionbar( "§cTeams verboten" );
            }
        } );

        //<editor-fold desc="PlayerJoinEvent">
        plugin.getEventManager( ).registerListener( PlayerJoinEvent.class, ( EventListener< PlayerJoinEvent > ) ( PlayerJoinEvent event ) -> {
            Player player = event.getPlayer( );
            setSpawn( player );
            player.teleport( plugin.getLocationManager( ).getSpawnLocation( ) );

            int online = plugin.getServer( ).getOnlinePlayers( ).size( );
            if ( online > 4 ) {
                event.setJoinMessage( null );
                return;
            }
            event.setJoinMessage( plugin.getPrefix( ) + VTabAPI.getChatPrefix( player.getUniqueId( ) ) + NickManager.getName( player ) + " §7hat das Spiel betreten§8. [§e" + online + "§8/§7" + player.getServer( ).getMaxPlayers( ) + "§8]" );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerQuitEvent">
        plugin.getEventManager( ).registerListener( PlayerQuitEvent.class, ( EventListener< PlayerQuitEvent > ) ( PlayerQuitEvent event ) -> {
            Player player = event.getPlayer( );
            User User = plugin.getUserManager( ).getUser( player );
            if ( User.hasLastDamager( ) )
                dead( player, User.getLastDamager( ) );

            int online = plugin.getServer( ).getOnlinePlayers( ).size( ) - 1;
            if ( online > 4 ) {
                event.setQuitMessage( null );
                return;
            }
            event.setQuitMessage( plugin.getPrefix( ) + VTabAPI.getChatPrefix( player.getUniqueId( ) ) + NickManager.getName( player ) + " §7hat das Spiel verlassen§8. [§e" + online + "§8/§7" + player.getServer( ).getMaxPlayers( ) + "§8]" );
        } );
        //</editor-fold>
        //<editor-fold desc="AsyncPlayerChatEvent">
        plugin.getEventManager( ).registerListener( AsyncPlayerChatEvent.class, ( EventListener< AsyncPlayerChatEvent > ) ( AsyncPlayerChatEvent event ) -> {
            Player player = event.getPlayer( );
            Stats stats = plugin.getUserManager( ).getUser( player ).getStats( );
            event.setFormat( "§8(§7" + stats.getPoints( ) + "§8) " + VTabAPI.getChatPrefix( player.getUniqueId( ) ) + NickManager.getName( player ) + " §8» §f" + event.getMessage( ).replace( "%", "%%" ) );
        } );
        //</editor-fold>
        //<editor-fold desc="EntityDamageEvent">
        plugin.getEventManager( ).registerListener( EntityDamageEvent.class, ( EventListener< EntityDamageEvent > ) ( EntityDamageEvent event ) -> {
            if ( !( event.getEntity( ) instanceof Player ) )
                return;
            if ( event.getCause( ) == EntityDamageEvent.DamageCause.FALL ) {
                event.setCancelled( true );
                return;
            }
            if ( event.getCause( ) == EntityDamageEvent.DamageCause.VOID ) {
                event.setCancelled( true );
                ( ( Player ) event.getEntity( ) ).setHealth( 0 );
                return;
            }
            if ( !plugin.getLocationManager( ).onSpawn( ( Player ) event.getEntity( ) ) )
                return;
            event.setCancelled( true );

        } );
        //</editor-fold>
        //<editor-fold desc="EntityDamageByEntityEvent">
        plugin.getEventManager( ).registerListener( EntityDamageByEntityEvent.class, ( EventListener< EntityDamageByEntityEvent > ) ( EntityDamageByEntityEvent event ) -> {
            if ( !( event.getEntity( ) instanceof Player ) )
                return;
            if ( plugin.getLocationManager( ).onSpawn( ( Player ) event.getEntity( ) ) ) {
                event.setCancelled( true );
                return;
            }
            if ( plugin.getLocationManager( ).onSpawn( event.getDamager( ).getLocation( ) ) ) {
                event.setCancelled( true );
                return;
            }
            Player player = ( Player ) event.getEntity( );
            Player damager = null;

            if ( event.getDamager( ) instanceof Player )
                damager = ( Player ) event.getDamager( );
            if ( event.getDamager( ) instanceof Arrow ) {
                Arrow arrow = ( Arrow ) event.getDamager( );
                if ( arrow.getShooter( ) instanceof Player )
                    damager = ( Player ) arrow.getShooter( );
            }
            if ( event.getDamager( ) instanceof FishHook ) {
                FishHook fishHook = ( FishHook ) event.getDamager( );
                if ( fishHook.getShooter( ) instanceof Player )
                    damager = ( Player ) fishHook.getShooter( );
            }

            if ( player.equals( damager ) )
                return;
            plugin.getUserManager( ).getUser( player ).setLastDamager( damager );
        } );
        //</editor-fold>
        //<editor-fold desc="FoodLevelChangeEvent">
        plugin.getEventManager( ).registerListener( FoodLevelChangeEvent.class, ( EventListener< FoodLevelChangeEvent > ) ( FoodLevelChangeEvent event ) -> {
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerPickupItemEvent">
        plugin.getEventManager( ).registerListener( PlayerPickupItemEvent.class, ( EventListener< PlayerPickupItemEvent > ) ( PlayerPickupItemEvent event ) -> {
            if ( plugin.getWorldManager( ).canBuild( event.getPlayer( ) ) )
                return;
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerDropItemEvent">
        plugin.getEventManager( ).registerListener( PlayerDropItemEvent.class, ( EventListener< PlayerDropItemEvent > ) ( PlayerDropItemEvent event ) -> {
            if ( plugin.getWorldManager( ).canBuild( event.getPlayer( ) ) )
                return;
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerInteractEvent">
        plugin.getEventManager( ).registerListener( PlayerInteractEvent.class, ( EventListener< PlayerInteractEvent > ) ( PlayerInteractEvent event ) -> {
            if ( event.getAction( ) != Action.PHYSICAL )
                return;
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerMoveEvent">
        plugin.getEventManager( ).registerListener( PlayerMoveEvent.class, ( EventListener< PlayerMoveEvent > ) ( PlayerMoveEvent event ) -> {
            Player player = event.getPlayer( );


            if ( !event.getTo( ).getBlock( ).getType( ).equals( Material.STATIONARY_WATER ) ) {
                if ( !playerOnWater.contains( player.getUniqueId( ) ) )
                    return;
                playerOnWater.remove( player.getUniqueId( ) );
                player.removePotionEffect( PotionEffectType.POISON );
                return;
            }
            if ( event.getTo( ).getBlock( ).getType( ).equals( Material.STATIONARY_WATER ) ) {
                if ( playerOnWater.contains( player.getUniqueId( ) ) )
                    return;
                playerOnWater.add( player.getUniqueId( ) );
                player.addPotionEffect( new PotionEffect( PotionEffectType.POISON, 99999, 1 ) );
                player.sendMessage( plugin.getPrefix( ) + "§cIm Wasser bekommst du Schaden§8." );
            }
        } );
        //</editor-fold>

        //<editor-fold desc="PlayerDeathEvent">
        plugin.getEventManager( ).registerListener( PlayerDeathEvent.class, ( EventListener< PlayerDeathEvent > ) ( PlayerDeathEvent event ) -> {
            Player player = event.getEntity( );
            event.setDeathMessage( null );
            if ( player.getKiller( ) != null ) {
                Player killer = player.getKiller( );
                dead( player, killer );
                return;
            }
            dead( player );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerRespawnEvent">
        plugin.getEventManager( ).registerListener( PlayerRespawnEvent.class, ( EventListener< PlayerRespawnEvent > ) ( PlayerRespawnEvent event ) -> {
            event.setRespawnLocation( plugin.getLocationManager( ).getSpawnLocation( ) );
            setSpawn( event.getPlayer( ) );
        } );
        //</editor-fold>
    }
    //</editor-fold>

    //<editor-fold desc="setIngame">
    public void setIngame( Player player ) {
        plugin.getLocationManager( ).getPlayerOnSpawn( ).remove( player );
        plugin.getInventoryManager( ).fillGameInventory( player );

        player.setHealth( 20 );
        player.setGameMode( GameMode.SURVIVAL );
        player.setLevel( 0 );
        player.setExp( 0 );

        player.setAllowFlight( false );

        for ( PotionEffect potionEffect : player.getActivePotionEffects( ) ) {
            player.removePotionEffect( potionEffect.getType( ) );
        }
    }
    //</editor-fold>

    //<editor-fold desc="setSpawn">
    public void setSpawn( Player player ) {
        plugin.getLocationManager( ).getPlayerOnSpawn( ).add( player );
        plugin.getInventoryManager( ).fillSpawnInventory( player );

        player.setHealth( 20 );
        player.setGameMode( GameMode.SURVIVAL );
        player.setLevel( 0 );
        player.setExp( 0 );

        player.setAllowFlight( false );

        for ( PotionEffect potionEffect : player.getActivePotionEffects( ) ) {
            player.removePotionEffect( potionEffect.getType( ) );
        }
    }
    //</editor-fold>

    //<editor-fold desc="dead">
    public void dead( Player player ) {
        User user = plugin.getUserManager( ).getUser( player );
        user.addDeath( );
        user.setPoints( user.getStats( ).getPoints( ) - 5 );
        user.setLastDamager( null );

        player.sendMessage( plugin.getPrefix( ) + "§cDu bist gestorben§8." );
        player.sendMessage( plugin.getPrefix( ) + "§c-§e5 §7Points" );
        player.playSound( player.getLocation( ), Sound.VILLAGER_DEATH, 1, 1 );

        plugin.runTaskLater( 5, ( ) -> {
            player.spigot( ).respawn( );
        } );
    }

    public void dead( Player player, Player killer ) {
        User user = plugin.getUserManager( ).getUser( player );
        user.addDeath( );
        user.setPoints( user.getStats( ).getPoints( ) - 5 );
        user.setLastDamager( null );

        User killerUser = plugin.getUserManager( ).getUser( killer );
        killerUser.addKill( );
        killerUser.setPoints( killerUser.getStats( ).getPoints( ) + 5 );

        killer.sendMessage( plugin.getPrefix( ) + "§7Du hast " + VTabAPI.getChatPrefix( player.getUniqueId( ) ) + NickManager.getName( player ) + " §7getötet§8." );
        killer.sendMessage( plugin.getPrefix( ) + "§a+§e5 §7Coins §8× §a+§e5 §7Points" );
        killer.playSound( killer.getLocation( ), Sound.LEVEL_UP, 16, 16 );
        killer.addPotionEffect( new PotionEffect( PotionEffectType.REGENERATION, 15, 5 ) );
        CoinApi.getApi( ).addCoins( killer.getUniqueId( ), 5 );
        plugin.getInventoryManager( ).addArrows( killer, 3 );
        plugin.getInventoryManager( ).resetFns( killer, 61 );

        int healt = ( int ) killer.getHealth( ) / 2;
        player.sendMessage( plugin.getPrefix( ) + "§7Du wurdest von " + VTabAPI.getChatPrefix( killer.getUniqueId( ) ) + NickManager.getName( killer ) + " §8[§7" + healt + " §c❤§8] §7getötet§8." );
        player.playSound( player.getLocation( ), Sound.VILLAGER_DEATH, 1, 1 );
        player.sendMessage( plugin.getPrefix( ) + "§c-§e5 §7Points" );

        plugin.runTaskLater( 5, ( ) -> {
            player.spigot( ).respawn( );
        } );
    }
    //</editor-fold>

    //<editor-fold desc="sendActionbar">
    public void sendActionbar( Player player, String message ) {
        PacketContainer packet = new PacketContainer( PacketType.Play.Server.CHAT );
        packet.getChatComponents( ).write( 0, WrappedChatComponent.fromJson( "{\"text\":\"" + message + "\"}" ) );
        packet.getBytes( ).write( 0, ( byte ) 2 );
        sendPacket( player, packet );
    }
    //</editor-fold>

    //<editor-fold desc="sendTitle">
    public void sendTitle( Player player, String titleString, String subtitleString ) {
        if ( titleString == null )
            titleString = "";
        if ( subtitleString == null )
            subtitleString = "";

        PacketContainer title = new PacketContainer( PacketType.Play.Server.TITLE );
        title.getTitleActions( ).write( 0, EnumWrappers.TitleAction.TITLE );
        title.getChatComponents( ).write( 0, WrappedChatComponent.fromJson( "{\"text\": \"" + titleString + "\"}" ) );
        title.getIntegers( ).write( 0, 9 ).write( 1, 17 ).write( 2, 3 );

        PacketContainer subtitle = new PacketContainer( PacketType.Play.Server.TITLE );
        subtitle.getTitleActions( ).write( 0, EnumWrappers.TitleAction.SUBTITLE );
        subtitle.getChatComponents( ).write( 0, WrappedChatComponent.fromJson( "{\"text\": \"" + subtitleString + "\"}" ) );
        subtitle.getIntegers( ).write( 0, 9 ).write( 1, 17 ).write( 2, 3 );

        sendPacket( player, title );
        sendPacket( player, subtitle );
    }
    //</editor-fold>

    //<editor-fold desc="sendPacket">
    public void sendPacket( Player player, Packet< ? > packet ) {
        ( ( CraftPlayer ) player ).getHandle( ).playerConnection.sendPacket( packet );
    }

    public void sendPacket( Player player, PacketContainer packetContainer ) {
        try {
            plugin.getProtocolManager( ).sendServerPacket( player, packetContainer );
        } catch ( InvocationTargetException ex ) {
            ex.printStackTrace( );
        }
    }
    //</editor-fold>

    //<editor-fold desc="checkInventoryClickEvent">
    public boolean checkInventoryClickEvent( InventoryClickEvent event ) {
        if ( !( event.getWhoClicked( ) instanceof Player ) )
            return false;
        Player player = ( Player ) event.getWhoClicked( );
        if ( event.getInventory( ) == null )
            return false;
        Inventory inventory = event.getInventory( );
        if ( inventory != event.getView( ).getTopInventory( ) )
            return false;
        if ( event.getCurrentItem( ) == null )
            return false;
        ItemStack itemStack = event.getCurrentItem( );
        if ( itemStack.getItemMeta( ) == null )
            return false;
        ItemMeta itemMeta = itemStack.getItemMeta( );
        if ( itemMeta.getDisplayName( ) == null )
            return false;
        return true;
    }
    //</editor-fold>

    //<editor-fold desc="checkPlayerInteractEvent">
    public boolean checkPlayerInteractEvent( PlayerInteractEvent event ) {
        if ( event.getPlayer( ) == null )
            return false;
        Player player = event.getPlayer( );
        if ( event.getAction( ) == null )
            return false;
        if ( player.getItemInHand( ) == null )
            return false;
        ItemStack itemStack = player.getItemInHand( );
        if ( itemStack.getItemMeta( ) == null )
            return false;
        ItemMeta itemMeta = itemStack.getItemMeta( );
        if ( itemMeta.getDisplayName( ) == null )
            return false;
        return true;
    }
    //</editor-fold>
}

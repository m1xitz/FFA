package de.m0xitz.ffa.managers;

import com.google.common.collect.Maps;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.Command;
import de.m0xitz.ffa.misc.CustomExecutor;
import de.m0xitz.ffa.misc.EventListener;
import de.m0xitz.ffa.misc.ReflectionUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.help.GenericCommandHelpTopic;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class EventManager {

    private Main plugin;

    private HashMap< EventListener, CopyOnWriteArrayList< CustomExecutor > > customExecutors;

    public EventManager( Main plugin ) {
        this.plugin = plugin;

        customExecutors = Maps.newHashMap( );
    }

    //<editor-fold desc="registerListener">
    public void registerListener( Class< ? extends Event > eventClass, EventListener eventListener ) {
        CustomExecutor customExecutor = new CustomExecutor( eventClass, eventListener );

        plugin.getServer( ).getPluginManager( ).registerEvent( eventClass, new Listener( ) {
        }, EventPriority.NORMAL, customExecutor, plugin );

        if ( !( customExecutors.containsKey( eventListener ) ) )
            customExecutors.put( eventListener, new CopyOnWriteArrayList<>( ) );
        customExecutors.get( eventListener ).add( customExecutor );
    }
    //</editor-fold>

    //<editor-fold desc="unregisterListener">
    public void unregisterListener( Class< ? extends Event > eventClass, EventListener eventListener ) {
        if ( !customExecutors.containsKey( eventListener ) )
            return;
        for ( CustomExecutor customExecutor : customExecutors.get( eventListener ) ) {
            if ( !customExecutor.getEventListener( ).equals( eventListener ) )
                continue;
            customExecutor.setDisable( true );
        }
    }
    //</editor-fold>

    //<editor-fold desc="onCommand">
    public void onCommand( Command command ) {
        SimpleCommandMap simpleCommandMap = ( SimpleCommandMap ) ReflectionUtils.get( Bukkit.getServer( ), "commandMap" );
        simpleCommandMap.register( plugin.getDescription( ).getName( ), command );
    }
    //</editor-fold>
}

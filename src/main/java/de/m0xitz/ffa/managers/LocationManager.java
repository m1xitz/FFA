package de.m0xitz.ffa.managers;

import com.google.common.collect.Lists;
import com.mojang.authlib.properties.Property;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.config.Config;
import de.m0xitz.ffa.misc.Command;
import de.m0xitz.ffa.misc.EventListener;
import de.m0xitz.ffa.misc.ItemBuilder;
import de.m0xitz.ffa.scenario.ScenarioType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class LocationManager {

    private Main plugin;

    private Config config;
    private List< Player > playerOnSpawn;

    private Location spawn;
    private Location holo;
    private Location pos1;
    private Location pos2;
    private int death;
    private Location min;
    private Location max;

    private List< Location > supplyDrops;
    private List< Location > zombieScenario;

    public LocationManager( Main plugin ) {
        this.plugin = plugin;

        config = plugin.getConfigManager( ).getConfig( "data" );
        playerOnSpawn = Lists.newArrayList( );
        supplyDrops = Lists.newArrayList( );
        zombieScenario = Lists.newArrayList( );

        //<editor-fold desc="Locations">
        if ( config.contains( "locations.spawn" ) ) {
            spawn = config.getLocation( "locations.spawn" );
            holo = config.getLocation( "locations.holo" );
            pos1 = config.getLocation( "locations.pos1" );
            pos2 = config.getLocation( "locations.pos2" );
            death = config.getInt( "locations.death" );
            min = config.getLocation( "locations.holo" );
            max = config.getLocation( "locations.holo" );
            if ( pos1.getY( ) < pos2.getY( ) ) {
                min = pos1;
                max = pos2;
            } else {
                min = pos2;
                max = pos1;
            }

            if ( config.contains( "supplydrop.place1" ) ) {
                for ( String key : config.getFileConfiguration( ).getKeys( true ) ) {
                    if ( !key.startsWith( "supplydrop.place" ) )
                        continue;
                    supplyDrops.add( config.getLocation( key ) );
                }

            }

            if ( config.contains( "scenario.zombie.place1" ) ) {
                for ( String key : config.getFileConfiguration( ).getKeys( true ) ) {
                    if ( !key.startsWith( "scenario.zombie.place" ) )
                        continue;
                    zombieScenario.add( config.getLocation( key ) );
                }
            }

            spawn.getWorld( ).setSpawnLocation( spawn.getBlockX( ), spawn.getBlockY( ), spawn.getBlockZ( ) );
        }
        //</editor-fold>

        init( );
    }

    //<editor-fold desc="init">
    private void init( ) {
        //<editor-fold desc="SetupCommand">
        plugin.getEventManager( ).onCommand( new Command( "setup" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( !player.hasPermission( "ffa.command.setup" ) ) {
                    player.sendMessage( plugin.getPrefix( ) + "§cFür diesen Command hast du keine Rechte!" );
                    return;
                }
                openSetupInventory( player );
            }
        } );
        //</editor-fold>
        //<editor-fold desc="SupplyDropCommand">
        plugin.getEventManager( ).onCommand( new Command( "supplydrop" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( !player.hasPermission( "ffa.command.setup" ) ) {
                    player.sendMessage( plugin.getPrefix( ) + "§cFür diesen Command hast du keine Rechte!" );
                    return;
                }
                if ( plugin.hasMetadata( player, "setupSupplyDrop" ) ) {
                    plugin.removeMetadata( player, "setupSupplyDrop" );

                    player.teleport( plugin.getLocationManager( ).getSpawnLocation( ) );
                    return;
                }
                plugin.setMetadata( player, "setupSupplyDrop", true );

                player.getInventory( ).clear( );
                player.getInventory( ).setItem( 0, new ItemBuilder( Material.CHEST ).setDisplayName( "§7Location §e#1" ).build( ) );
            }
        } );
        //</editor-fold>
        //<editor-fold desc="ScenarioCommand">
        plugin.getEventManager( ).onCommand( new Command( "scenario" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( !player.hasPermission( "ffa.command.setup" ) ) {
                    player.sendMessage( plugin.getPrefix( ) + "§cFür diesen Command hast du keine Rechte!" );
                    return;
                }
                if ( strings.length == 1 ) {
                    ScenarioType scenarioType = ScenarioType.ZOMBIE;
                    if ( plugin.hasMetadata( player, "scenario" ) ) {
                        plugin.removeMetadata( player, "scenario" );

                        player.teleport( plugin.getLocationManager( ).getSpawnLocation( ) );
                        return;
                    }
                    plugin.setMetadata( player, "scenario", scenarioType );
                    player.getInventory( ).clear( );
                    player.getInventory( ).setItem( 0, new ItemBuilder( Material.SKULL_ITEM ).setDisplayName( "§7Location §e#1" ).build( ) );
                    return;
                }
                player.sendMessage( plugin.getPrefix( ) + "§cNutze§8: §8/§cscenario §7[§fType§7]" );
            }
        } );
        //</editor-fold>

        //<editor-fold desc="InventoryClickEvent">
        plugin.getEventManager( ).registerListener( InventoryClickEvent.class, ( EventListener< InventoryClickEvent > ) ( InventoryClickEvent event ) -> {
            if ( !plugin.getPlayerManager( ).checkInventoryClickEvent( event ) )
                return;
            Player player = ( Player ) event.getWhoClicked( );
            Inventory inventory = event.getInventory( );
            ItemStack itemStack = event.getCurrentItem( );
            ItemMeta itemMeta = itemStack.getItemMeta( );

            if ( !inventory.getName( ).equals( "§9FFA §8➟ §7Setup" ) )
                return;
            event.setCancelled( true );

            if ( itemMeta.getDisplayName( ).equals( "§7Setze§8: §eSpawn" ) ) {
                config.set( "locations.spawn", player.getLocation( ) );
                config.save( );
                player.sendMessage( plugin.getPrefix( ) + "§7Du hast erfolgreich die Location §eSpawn §7abgespeichert§8." );
                openSetupInventory( player );
                return;
            }
            if ( itemMeta.getDisplayName( ).equals( "§7Setze§8: §eHologram" ) ) {
                config.set( "locations.holo", player.getLocation( ) );
                config.save( );
                player.sendMessage( plugin.getPrefix( ) + "§7Du hast erfolgreich die Location §eHologram §7abgespeichert§8." );
                openSetupInventory( player );
                return;
            }
            if ( itemMeta.getDisplayName( ).equals( "§7Setze§8: §ePosition 1" ) ) {
                config.set( "locations.pos1", player.getLocation( ) );
                config.save( );
                player.sendMessage( plugin.getPrefix( ) + "§7Du hast erfolgreich die Location §ePosition 1 §7abgespeichert§8." );
                openSetupInventory( player );
                return;
            }
            if ( itemMeta.getDisplayName( ).equals( "§7Setze§8: §ePosition 2" ) ) {
                config.set( "locations.pos2", player.getLocation( ) );
                config.save( );
                player.sendMessage( plugin.getPrefix( ) + "§7Du hast erfolgreich die Location §ePosition 2 §7abgespeichert§8." );
                openSetupInventory( player );
                return;
            }
            if ( !itemMeta.getDisplayName( ).equals( "§7Setze§8: §eMindesthöhe" ) )
                return;
            config.set( "locations.death", player.getLocation( ) );
            config.save( );
            player.sendMessage( plugin.getPrefix( ) + "§7Du hast erfolgreich die Location §eMindesthöhe §7abgespeichert§8." );
            openSetupInventory( player );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerMoveEvent">
        plugin.getEventManager( ).registerListener( PlayerMoveEvent.class, ( EventListener< PlayerMoveEvent > ) ( PlayerMoveEvent event ) -> {
            Player player = event.getPlayer( );
            if ( plugin.getSpectatorManager( ).isSpectator( player ) )
                return;
            if ( event.getFrom( ).getBlockY( ) == death ) {
                player.setHealth( 0 );
                return;
            }
            if ( onSpawn( player.getLocation( ) ) ) {
                if ( playerOnSpawn.contains( player ) )
                    return;
                plugin.getPlayerManager( ).setSpawn( player );
                return;
            }
            if ( !playerOnSpawn.contains( player ) )
                return;
            plugin.getPlayerManager( ).setIngame( player );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerJoinEvent">
        plugin.getEventManager( ).registerListener( PlayerJoinEvent.class, ( EventListener< PlayerJoinEvent > ) ( PlayerJoinEvent event ) -> {
            Player player = event.getPlayer( );
            if ( playerOnSpawn.contains( player ) )
                return;
            playerOnSpawn.add( player );
        } );
        //</editor-fold>
        //<editor-fold desc="PlayerQuitEvent">
        plugin.getEventManager( ).registerListener( PlayerQuitEvent.class, ( EventListener< PlayerQuitEvent > ) ( PlayerQuitEvent event ) -> {
            Player player = event.getPlayer( );
            if ( playerOnSpawn.contains( player ) )
                playerOnSpawn.remove( player );
            if ( plugin.hasMetadata( player, "setupTop10" ) )
                plugin.removeMetadata( player, "setupTop10" );
        } );
        //</editor-fold>
        //<editor-fold desc="BlockPlaceEvent - SupplyDrop">
        plugin.getEventManager( ).registerListener( BlockPlaceEvent.class, ( EventListener< BlockPlaceEvent > ) ( BlockPlaceEvent event ) -> {
            Player player = event.getPlayer( );
            if ( !plugin.hasMetadata( player, "setupSupplyDrop" ) )
                return;
            if ( player.getItemInHand( ) == null )
                return;
            ItemStack itemStack = player.getItemInHand( );
            if ( itemStack.getItemMeta( ) == null )
                return;
            ItemMeta itemMeta = itemStack.getItemMeta( );
            if ( itemMeta.getDisplayName( ) == null )
                return;
            if ( !itemMeta.getDisplayName( ).startsWith( "§7Location §e#" ) )
                return;
            int i = Integer.parseInt( itemMeta.getDisplayName( ).replace( "§7Location §e#", "" ) );
            player.sendMessage( "Location " + i + " wurde gespeichert" );
            player.getInventory( ).setItem( 0, chest( i + 1 ) );
            Block chestBlock = event.getBlock( );
            chestBlock.setType( Material.AIR );
            chestBlock.getLocation( ).getWorld( ).strikeLightningEffect( chestBlock.getLocation( ) );

            config.set( "supplydrop.place" + i, chestBlock.getLocation( ) );
            config.save( );
        } );
        //</editor-fold>
        //<editor-fold desc="BlockPlaceEvent - Scenario">
        plugin.getEventManager( ).registerListener( BlockPlaceEvent.class, ( EventListener< BlockPlaceEvent > ) ( BlockPlaceEvent event ) -> {
            Player player = event.getPlayer( );
            if ( !plugin.hasMetadata( player, "scenario" ) )
                return;
            if ( player.getItemInHand( ) == null )
                return;
            ItemStack itemStack = player.getItemInHand( );
            if ( itemStack.getItemMeta( ) == null )
                return;
            ItemMeta itemMeta = itemStack.getItemMeta( );
            if ( itemMeta.getDisplayName( ) == null )
                return;
            if ( !itemMeta.getDisplayName( ).startsWith( "§7Location §e#" ) )
                return;
            int i = Integer.parseInt( itemMeta.getDisplayName( ).replace( "§7Location §e#", "" ) );
            player.sendMessage( "Location " + i + " wurde gespeichert" );
            player.getInventory( ).setItem( 0, scenario( i + 1 ) );
            Block chestBlock = event.getBlock( );
            chestBlock.setType( Material.AIR );
            chestBlock.getLocation( ).getWorld( ).strikeLightningEffect( chestBlock.getLocation( ) );

            ScenarioType scenarioType = ( ScenarioType ) plugin.getMetadata( player, "scenario" );

            config.set( "scenario." + scenarioType.name( ).toLowerCase( ) + ".place" + i, chestBlock.getLocation( ) );
            config.save( );
        } );
        //</editor-fold>
    }
    //</editor-fold>

    //<editor-fold desc="openSetupInventory">
    public void openSetupInventory( Player player ) {
        Inventory inventory = plugin.getServer( ).createInventory( null, 9 * 3, "§9FFA §8➟ §7Setup" );
        player.openInventory( inventory );

        ItemBuilder base = new ItemBuilder( Material.STAINED_GLASS_PANE, 1, ( short ) 15 );
        base.setDisplayName( "§1" );

        ItemBuilder spawn = new ItemBuilder( Material.GOLD_PLATE );
        spawn.setDisplayName( "§7Setze§8: §eSpawn" );
        if ( config.contains( "locations.spawn" ) )
            spawn.setLore( "§a✔" );

        ItemBuilder holo = new ItemBuilder( Material.ARMOR_STAND );
        holo.setDisplayName( "§7Setze§8: §eHologram" );
        if ( config.contains( "locations.holo" ) )
            holo.setLore( "§a✔" );

        ItemBuilder pos1 = new ItemBuilder( Material.SKULL_ITEM );
        pos1.setDisplayName( "§7Setze§8: §ePosition 1" );
        pos1.setOwner( new Property( "textures", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2ViZjkwNzQ5NGE5MzVlOTU1YmZjYWRhYjgxYmVhZmI5MGZiOWJlNDljNzAyNmJhOTdkNzk4ZDVmMWEyMyJ9fX0=" ) );
        if ( config.contains( "locations.pos1" ) )
            pos1.setLore( "§a✔" );

        ItemBuilder pos2 = new ItemBuilder( Material.SKULL_ITEM );
        pos2.setDisplayName( "§7Setze§8: §ePosition 2" );
        pos2.setOwnerUrl( "http://textures.minecraft.net/texture/1b6f1a25b6bc199946472aedb370522584ff6f4e83221e5946bd2e41b5ca13b" );
        if ( config.contains( "locations.pos2" ) )
            pos2.setLore( "§a✔" );

        ItemBuilder death = new ItemBuilder( Material.SKULL_ITEM );
        death.setDisplayName( "§7Setze§8: §eMindesthöhe" );
        death.setOwner( new Property( "textures", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmRhZGQ3NTVkMDg1MzczNTJiZjdhOTNlM2JiN2RkNGQ3MzMxMjFkMzlmMmZiNjcwNzNjZDQ3MWY1NjExOTRkZCJ9fX0=" ) );
        if ( config.contains( "locations.death" ) )
            death.setLore( "§a✔" );

        for ( int i = 0; i < inventory.getSize( ); i++ ) {
            inventory.setItem( i, base.build( ) );
        }

        inventory.setItem( 10, spawn.build( ) );
        inventory.setItem( 11, holo.build( ) );
        inventory.setItem( 13, pos1.build( ) );
        inventory.setItem( 14, pos2.build( ) );
        inventory.setItem( 16, death.build( ) );

        player.updateInventory( );
    }
    //</editor-fold>

    //<editor-fold desc="getLocation">
    public Location getSpawnLocation( ) {
        return spawn;
    }

    public Location getHologramLocation( ) {
        return holo;
    }

    public Location getPositionOneLocation( ) {
        return pos1;
    }

    public Location getPositionTwoLocation( ) {
        return pos2;
    }

    public int getDeathLocation( ) {
        return death;
    }

    public Location getMinLocation( ) {
        return min;
    }

    public Location getMaxLocation( ) {
        return max;
    }

    public List< Location > getSupplyDropLocations( ) {
        return supplyDrops;
    }

    public List< Location > getZombieScenario( ) {
        return zombieScenario;
    }
    //</editor-fold>

    //<editor-fold desc="onSpawn">
    public boolean onSpawn( Location location ) {
        int x = location.getBlockX( );
        int y = location.getBlockY( );
        int z = location.getBlockZ( );
        int x1 = Math.min( min.getBlockX( ), max.getBlockX( ) );
        int y1 = Math.min( min.getBlockY( ), max.getBlockY( ) );
        int z1 = Math.min( min.getBlockZ( ), max.getBlockZ( ) );
        int x2 = Math.max( min.getBlockX( ), max.getBlockX( ) );
        int y2 = Math.max( min.getBlockY( ), max.getBlockY( ) );
        int z2 = Math.max( min.getBlockZ( ), max.getBlockZ( ) );
        return x >= x1 && x <= x2 && y >= y1 && y <= y2 && z >= z1 && z <= z2;
    }

    public boolean onSpawn( Player player ) {
        return playerOnSpawn.contains( player );
    }
    //</editor-fold>

    //<editor-fold desc="getPlayerOnSpawn">
    public List< Player > getPlayerOnSpawn( ) {
        return playerOnSpawn;
    }
    //</editor-fold>

    //<editor-fold desc="getCircle">
    public  List< Location > getCircle( Location center, double radius, int amount ) {
        World world = center.getWorld( );
        double increment = 6.283185307179586 / ( double ) amount;
        List< Location > locations = new ArrayList<>( );
        int i = 0;
        while ( i < amount ) {
            double angle = ( double ) i * increment;
            double x = center.getX( ) + radius * Math.cos( angle );
            double z = center.getZ( ) + radius * Math.sin( angle );
            locations.add( new Location( world, x, center.getY( ), z ) );
            ++i;
        }
        return locations;
    }
    //</editor-fold>

    //<editor-fold desc="skull">
    private ItemStack skull( int i ) {
        ItemBuilder itemBuilder = new ItemBuilder( Material.SKULL_ITEM );
        itemBuilder.setDisplayName( "§7Platz §e#" + i );

        switch ( i ) {
            case 2:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/cc596a41daea51be2e9fec7de2d89068e2fa61c9d57a8bdde44b55937b6037" );
                break;
            case 3:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/b85d4fda56bfeb85124460ff72b251dca8d1deb6578070d612b2d3adbf5a8" );
                break;
            case 4:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/3852a25fe69ca86fb982fb3cc7ac9793f7356b50b92cb0e193d6b4632a9bd629" );
                break;
            case 5:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/74ee7d954eb14a5ccd346266231bf9a6716527b59bbcd7956cef04a9d9b" );
                break;
            case 6:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/2682a3ae948374e037e3d7dd687d59d185dd2cc8fc09dfeb42f98f8d259e5c3" );
                break;
            case 7:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/4ea30c24c60b3bc1af658ef661b771c48d5b9c9e28188cf9de9f832422e510" );
                break;
            case 8:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/66abafd023f230e4485aaf26e19368f5980d4f14a59fcc6d11a4466994892" );
                break;
            case 9:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/8d7910e10334f890a625483ac0c824b5e4a1a4b15a956327a3e3ae458d9ea4" );
                break;
            case 10:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/9115a8861d6557d2873fec82327ea1d4643e2ff191dbf1ef919f94a545" );
                break;
            default:
                itemBuilder.setOwnerUrl( "http://textures.minecraft.net/texture/a0a19e23d21f2db063cc55e99ae874dc8b23be779be34e52e7c7b9a25" );
                break;
        }

        return itemBuilder.build( );
    }
    //</editor-fold>

    //<editor-fold desc="chest">
    private ItemStack chest( int i ) {
        ItemBuilder itemBuilder = new ItemBuilder( Material.CHEST );
        itemBuilder.setDisplayName( "§7Location §e#" + i );
        return itemBuilder.build( );
    }
    //</editor-fold>

    //<editor-fold desc="scenario">
    private ItemStack scenario( int i ) {
        ItemBuilder itemBuilder = new ItemBuilder( Material.SKULL_ITEM );
        itemBuilder.setDisplayName( "§7Location §e#" + i );
        return itemBuilder.build( );
    }
    //</editor-fold>
}

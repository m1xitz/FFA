package de.m0xitz.ffa.managers;

import com.google.common.collect.Lists;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.Command;
import de.m0xitz.ffa.misc.EventListener;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class SpectatorManager {

    private Main plugin;
    private List< Player > specs;

    public SpectatorManager( Main plugin ) {
        this.plugin = plugin;

        specs = Lists.newArrayList( );
        init( );
    }

    private void init( ) {
        //<editor-fold desc="SpectatorCommand">
        plugin.getEventManager( ).onCommand( new Command( "spectator", "spec" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( !player.hasPermission( "ffa.command.spectator" ) ) {
                    player.sendMessage( plugin.getPrefix( ) + "§cFür diesen Command hast du keine Rechte!" );
                    return;
                }
                if ( strings.length == 1 ) {
                    if ( isSpectator( player ) ) {
                        return;
                    }
                    setSpectator( player );
                    return;
                }
                if ( isSpectator( player ) ) {
                    removeSpectator( player, false );
                    return;
                }
                setSpectator( player );
            }
        } );
        //</editor-fold>

        //<editor-fold desc="PlayerQuitEvent">
        plugin.getEventManager( ).registerListener( PlayerQuitEvent.class, ( EventListener< PlayerQuitEvent > ) ( PlayerQuitEvent event ) -> {
            Player player = event.getPlayer( );

            if ( !isSpectator( player ) )
                return;
            removeSpectator( player, true );
        } );
        //</editor-fold>
        //<editor-fold desc="AsyncPlayerChatEvent">
        plugin.getEventManager( ).registerListener( AsyncPlayerChatEvent.class, ( EventListener< AsyncPlayerChatEvent > ) ( AsyncPlayerChatEvent event ) -> {
            Player player = event.getPlayer( );
            if ( !isSpectator( player ) )
                return;
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="InventoryClickEvent">
        plugin.getEventManager( ).registerListener( InventoryClickEvent.class, ( EventListener< InventoryClickEvent > ) ( InventoryClickEvent event ) -> {
            if ( !plugin.getPlayerManager( ).checkInventoryClickEvent( event ) )
                return;
            if ( !isSpectator( ( Player ) event.getWhoClicked( ) ) )
                return;
            event.setCancelled( true );
        } );
        //</editor-fold>
    }

    //<editor-fold desc="setSpectator">
    public void setSpectator( Player player ) {
        specs.add( player );
        player.sendMessage( plugin.getPrefix( ) + "§7Du bist jetzt als §eSpectator §7unterwegs§8." );
        for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
            all.hidePlayer( player );
        }

        player.addPotionEffect( new PotionEffect( PotionEffectType.INVISIBILITY, 99999, 255, true, false ) );

        player.spigot( ).setCollidesWithEntities( false );
        player.setAllowFlight( true );

        player.getInventory( ).clear( );
        player.getInventory( ).setArmorContents( null );
        player.updateInventory( );
        player.getInventory( ).setItem( 0, plugin.getInventoryManager( ).getSpecItem( ).build( ) );
    }
    //</editor-fold>

    //<editor-fold desc="removeSpectator">
    public void removeSpectator( Player player, boolean quit ) {
        specs.remove( player );
        player.sendMessage( plugin.getPrefix( ) + "§7Du bist wieder als §eSpieler §7unterwegs§8." );
        for ( Player all : plugin.getServer( ).getOnlinePlayers( ) ) {
            all.showPlayer( player );
        }

        player.spigot( ).setCollidesWithEntities( true );

        if ( quit )
            return;
        player.teleport( plugin.getLocationManager( ).getSpawnLocation( ) );
        plugin.getPlayerManager( ).setSpawn( player );
    }
    //</editor-fold>

    //<editor-fold desc="isSpectator">
    public boolean isSpectator( Player player ) {
        return specs.contains( player );
    }
    //</editor-fold>

    //<editor-fold desc="getSpecs">
    public List< Player > getSpecs( ) {
        return specs;
    }
    //</editor-fold>
}

package de.m0xitz.ffa.managers;

import com.google.common.collect.Lists;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.misc.Command;
import de.m0xitz.ffa.misc.EventListener;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.List;

import static org.bukkit.Material.AIR;

public class WorldManager {

    private Main plugin;

    private final List< Material > plants = Lists.newArrayList( Material.CROPS, Material.POTATO, Material.CARROT, Material.NETHER_WARTS, Material.YELLOW_FLOWER, Material.RED_ROSE, Material.LONG_GRASS, Material.DOUBLE_PLANT );

    public WorldManager( Main plugin ) {
        this.plugin = plugin;

        init( );
    }

    //<editor-fold desc="init">
    private void init( ) {
        //<editor-fold desc="BuildCommand">
        plugin.getEventManager( ).onCommand( new Command( "build", "edit" ) {
            @Override
            public void onPlayer( Player player, String[] strings ) {
                if ( !player.hasPermission( "ffa.command.build" ) ) {
                    player.sendMessage( plugin.getPrefix( ) + "§cFür diesen Command hast du keine Rechte!" );
                    return;
                }
                if ( plugin.hasMetadata( player, "build" ) ) {
                    plugin.removeMetadata( player, "build" );
                    player.sendMessage( plugin.getPrefix( ) + "§7Du kannst jetzt nicht mehr §ebauen§8." );
                    return;
                }
                plugin.setMetadata( player, "build", true );
                player.sendMessage( plugin.getPrefix( ) + "§7Du kannst jetzt §ebauen§8." );
            }
        } );
        //</editor-fold>

        //<editor-fold desc="BlockBreakEvent">
        plugin.getEventManager( ).registerListener( BlockBreakEvent.class, ( EventListener< BlockBreakEvent > ) ( BlockBreakEvent event ) -> {
            if ( canBuild( event.getPlayer( ) ) )
                return;

            if ( !plants.contains( event.getBlock( ).getType( ) ) ) {
                event.setCancelled( true );
                return;
            }
            event.setCancelled( true );
            Block block = event.getBlock( );
            Material material = block.getType( );
            byte data = block.getData( );

            block.setType( AIR );

            plugin.runTaskLater( 20 * 3, ( ) -> {
                block.setType( material );
                block.setData( data );
                block.getLocation( ).getWorld( ).playSound( block.getLocation( ), Sound.CHICKEN_EGG_POP, 1, 1 );
            } );
        } );
        //</editor-fold>
        //<editor-fold desc="BlockPlaceEvent">
        plugin.getEventManager( ).registerListener( BlockPlaceEvent.class, ( EventListener< BlockPlaceEvent > ) ( BlockPlaceEvent event ) -> {
            if ( event.getBlock( ).getType( ) == Material.TNT ) {
                Location location = event.getBlock( ).getLocation( );
                event.getBlock( ).setType( AIR );

                TNTPrimed tnt = location.getWorld( ).spawn( location, TNTPrimed.class );
                tnt.setFuseTicks( 30 );
                return;
            }
            if ( event.getBlock( ).getType( ) == Material.FIRE )
                return;
            if ( canBuild( event.getPlayer( ) ) )
                return;
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="EntitySpawnEvent">
        plugin.getEventManager( ).registerListener( CreatureSpawnEvent.class, ( EventListener< CreatureSpawnEvent > ) ( CreatureSpawnEvent event ) -> {
            Entity entity = event.getEntity( );
            if ( entity instanceof Player || entity instanceof Arrow || entity instanceof FallingBlock || entity instanceof TNTPrimed )
                return;
            event.setCancelled( true );
            entity.remove( );
        } );
        //</editor-fold>
        //<editor-fold desc="WeatherChangeEvent">
        plugin.getEventManager( ).registerListener( WeatherChangeEvent.class, ( EventListener< WeatherChangeEvent > ) ( WeatherChangeEvent event ) -> {
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="ThunderChangeEvent">
        plugin.getEventManager( ).registerListener( ThunderChangeEvent.class, ( EventListener< ThunderChangeEvent > ) ( ThunderChangeEvent event ) -> {
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="EntityExplodeEvent">
        plugin.getEventManager( ).registerListener( EntityExplodeEvent.class, ( EventListener< EntityExplodeEvent > ) ( EntityExplodeEvent event ) -> {
            event.blockList( ).clear( );
        } );
        //</editor-fold>
        //<editor-fold desc="BlockBurnEvent">
        plugin.getEventManager( ).registerListener( BlockBurnEvent.class, ( EventListener< BlockBurnEvent > ) ( BlockBurnEvent event ) -> {
            event.setCancelled( true );
        } );
        //</editor-fold>
        //<editor-fold desc="LeavesDecayEvent">
        plugin.getEventManager( ).registerListener( LeavesDecayEvent.class, ( EventListener< LeavesDecayEvent > ) ( LeavesDecayEvent event ) -> {
            event.setCancelled( true );
        } );
        //</editor-fold>
    }
    //</editor-fold>

    //<editor-fold desc="canBuild">
    public boolean canBuild( Player player ) {
        return plugin.hasMetadata( player, "build" );
    }
    //</editor-fold>
}

package de.m0xitz.ffa.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class KillstreakChangeEvent extends PlayerEvent {

    private static final HandlerList handlers = new HandlerList( );
    private Action action;

    private long oldKillstreak;
    private long newKillstreak;

    public KillstreakChangeEvent( Player player, Action action, long oldKillstreak, long newKillstreak ) {
        super( player );
        this.action = action;
        this.oldKillstreak = oldKillstreak;
        this.newKillstreak = newKillstreak;
    }

    //<editor-fold desc="getAction">
    public Action getAction( ) {
        return action;
    }
    //</editor-fold>

    //<editor-fold desc="setAction">
    public void setAction( Action action ) {
        this.action = action;
    }
    //</editor-fold>

    //<editor-fold desc="getOldKillstreak">
    public long getOldKillstreak( ) {
        return oldKillstreak;
    }
    //</editor-fold>

    //<editor-fold desc="getNewKillstreak">
    public long getNewKillstreak( ) {
        return newKillstreak;
    }
    //</editor-fold>

    //<editor-fold desc="getHandlers">
    @Override
    public HandlerList getHandlers( ) {
        return handlers;
    }
    //</editor-fold>

    //<editor-fold desc="getHandlerList">
    public static HandlerList getHandlerList( ) {
        return handlers;
    }
    //</editor-fold>

    //<editor-fold desc="Action">
    public enum Action {
        ADD,
        CLEAR
    }
    //</editor-fold>
}

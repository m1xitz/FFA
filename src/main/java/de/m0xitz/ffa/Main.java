package de.m0xitz.ffa;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.dytanic.cloudnet.bridge.CloudServer;
import de.m0xitz.ffa.config.ConfigManager;
import de.m0xitz.ffa.database.DatabaseManager;
import de.m0xitz.ffa.managers.*;
import de.m0xitz.ffa.misc.AnimatedScoreboard;
import de.m0xitz.ffa.supplydrop.SupplyDropManager;
import de.m0xitz.ffa.scenario.ScenarioManager;
import de.m0xitz.ffa.user.UserManager;
import de.m0xitz.ffa.user.inventory.InventoryManager;
import de.m0xitz.ffa.user.stats.StatsManager;
import de.ncrypted.vtab.manager.VTabAPI;
import de.slikey.effectlib.EffectManager;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class Main extends JavaPlugin {

    private final String prefix = "§8» §9FFA §8┃ §7";
    private ProtocolManager protocolManager;
    private EffectManager effectManager;

    private ConfigManager configManager;
    private DatabaseManager databaseManager;
    private EventManager eventManager;
    private PlayerManager playerManager;
    private WorldManager worldManager;
    private SpectatorManager spectatorManager;
    private LocationManager locationManager;
    private UserManager userManager;
    private StatsManager statsManager;
    private InventoryManager inventoryManager;
    private SupplyDropManager supplyDropManager;
    private ScenarioManager scenarioManager;

    private AnimatedScoreboard animatedScoreboard;

    @Override
    public void onEnable( ) {
        protocolManager = ProtocolLibrary.getProtocolManager( );
        effectManager = new EffectManager( this );

        configManager = new ConfigManager( this );
        databaseManager = new DatabaseManager( this );
        eventManager = new EventManager( this );
        playerManager = new PlayerManager( this );
        worldManager = new WorldManager( this );
        spectatorManager = new SpectatorManager( this );
        locationManager = new LocationManager( this );
        userManager = new UserManager( this );
        statsManager = new StatsManager( this );
        inventoryManager = new InventoryManager( this );
        supplyDropManager = new SupplyDropManager( this );
        scenarioManager = new ScenarioManager( this );

        animatedScoreboard = new AnimatedScoreboard( this );
        VTabAPI.enableChatsystem( false );


        CloudServer.getInstance( ).setMotd( "Fallen Drill" );
        CloudServer.getInstance( ).setAllowAutoStart( false );
        CloudServer.getInstance( ).setMaxPlayers( 32 );
        CloudServer.getInstance( ).update( );
    }

    @Override
    public void onDisable( ) {
        databaseManager.closeConnection( );
    }

    //<editor-fold desc="getPrefix">
    public String getPrefix( ) {
        return prefix;
    }
    //</editor-fold>

    //<editor-fold desc="getProtocolManager">
    public ProtocolManager getProtocolManager( ) {
        return protocolManager;
    }
    //</editor-fold>

    //<editor-fold desc="getEffectManager">
    public EffectManager getEffectManager( ) {
        return effectManager;
    }
    //</editor-fold>

    //<editor-fold desc="getConfigManager">
    public ConfigManager getConfigManager( ) {
        return configManager;
    }
    //</editor-fold>

    //<editor-fold desc="getDatabaseManager">
    public DatabaseManager getDatabaseManager( ) {
        return databaseManager;
    }
    //</editor-fold>

    //<editor-fold desc="getEventManager">
    public EventManager getEventManager( ) {
        return eventManager;
    }
    //</editor-fold>

    //<editor-fold desc="getPlayerManager">
    public PlayerManager getPlayerManager( ) {
        return playerManager;
    }
    //</editor-fold>

    //<editor-fold desc="getWorldManager">
    public WorldManager getWorldManager( ) {
        return worldManager;
    }
    //</editor-fold>

    //<editor-fold desc="getSpectatorManager">
    public SpectatorManager getSpectatorManager( ) {
        return spectatorManager;
    }
    //</editor-fold>

    //<editor-fold desc="getLocationManager">
    public LocationManager getLocationManager( ) {
        return locationManager;
    }
    //</editor-fold>

    //<editor-fold desc="getUserManager">
    public UserManager getUserManager( ) {
        return userManager;
    }
    //</editor-fold>

    //<editor-fold desc="getStatsManager">
    public StatsManager getStatsManager( ) {
        return statsManager;
    }
    //</editor-fold>

    //<editor-fold desc="getInventoryManager">
    public InventoryManager getInventoryManager( ) {
        return inventoryManager;
    }
    //</editor-fold>

    //<editor-fold desc="getSupplyDropManager">
    public SupplyDropManager getSupplyDropManager( ) {
        return supplyDropManager;
    }
    //</editor-fold>

    //<editor-fold desc="getScenarioManager">
    public ScenarioManager getScenarioManager( ) {
        return scenarioManager;
    }
    //</editor-fold>

    //<editor-fold desc="getAnimatedScoreboard">
    public AnimatedScoreboard getAnimatedScoreboard( ) {
        return animatedScoreboard;
    }
    //</editor-fold>


    //<editor-fold desc="setMetadata">
    public void setMetadata( Entity entity, String key, Object value ) {
        if ( hasMetadata( entity, key ) )
            removeMetadata( entity, key );
        entity.setMetadata( key, new FixedMetadataValue( this, value ) );
    }

    public void setMetadata( Block block, String key, Object value ) {
        if ( hasMetadata( block, key ) )
            removeMetadata( block, key );
        block.setMetadata( key, new FixedMetadataValue( this, value ) );
    }
    //</editor-fold>

    //<editor-fold desc="removeMetadata">
    public Object removeMetadata( Entity entity, String key ) {
        if ( !hasMetadata( entity, key ) )
            return null;
        Object object = entity.getMetadata( key );
        entity.removeMetadata( key, this );
        return object;
    }

    public Object removeMetadata( Block block, String key ) {
        if ( !hasMetadata( block, key ) )
            return null;
        Object object = block.getMetadata( key );
        block.removeMetadata( key, this );
        return object;
    }
    //</editor-fold>

    //<editor-fold desc="getMetadata">
    public Object getMetadata( Entity entity, String key ) {
        if ( !hasMetadata( entity, key ) )
            return null;
        return entity.getMetadata( key ).get( 0 ).value( );
    }

    public Object getMetadata( Block block, String key ) {
        if ( !hasMetadata( block, key ) )
            return null;
        return block.getMetadata( key ).get( 0 ).value( );
    }
    //</editor-fold>

    //<editor-fold desc="hasMetadata">
    public boolean hasMetadata( Block block, String key ) {
        return block.hasMetadata( key );
    }

    public boolean hasMetadata( Entity entity, String key ) {
        return entity.hasMetadata( key );
    }
    //</editor-fold>


    //<editor-fold desc="runTask">
    public BukkitTask runTask( Runnable runnable ) {
        return getServer( ).getScheduler( ).runTask( this, runnable );
    }
    //</editor-fold>

    //<editor-fold desc="runTaskAsync">
    public BukkitTask runTaskAsync( Runnable runnable ) {
        return getServer( ).getScheduler( ).runTaskAsynchronously( this, runnable );
    }
    //</editor-fold>

    //<editor-fold desc="runTaskLater">
    public BukkitTask runTaskLater( long delay, Runnable runnable ) {
        return getServer( ).getScheduler( ).runTaskLater( this, runnable, delay );
    }
    //</editor-fold>

    //<editor-fold desc="runTaskLaterAsync">
    public BukkitTask runTaskLaterAsync( long delay, Runnable runnable ) {
        return getServer( ).getScheduler( ).runTaskLaterAsynchronously( this, runnable, delay );
    }
    //</editor-fold>

    //<editor-fold desc="runTaskTimer">
    public BukkitTask runTaskTimer( long delay, long repeat, Runnable runnable ) {
        return getServer( ).getScheduler( ).runTaskTimer( this, runnable, delay, repeat );
    }
    //</editor-fold>

    //<editor-fold desc="runTaskTimerAsync">
    public BukkitTask runTaskTimerAsync( long delay, long repeat, Runnable runnable ) {
        return getServer( ).getScheduler( ).runTaskTimerAsynchronously( this, runnable, delay, repeat );
    }
    //</editor-fold>
}
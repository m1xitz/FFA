package de.m0xitz.ffa.database;

import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.config.Config;

public class DatabaseManager {

    private Main plugin;

    private String host;
    private String databaseString;
    private String user;
    private String password;

    private Database database;

    public DatabaseManager( Main plugin ) {
        this.plugin = plugin;

        Config config = plugin.getConfigManager( ).getConfig( "config" );

        config.addDefaults( "mysql.host", "host" );
        config.addDefaults( "mysql.database", "database" );
        config.addDefaults( "mysql.user", "user" );
        config.addDefaults( "mysql.password", "password" );
        config.save( );

        host = config.getString( "mysql.host" );
        user = config.getString( "mysql.user" );
        password = config.getString( "mysql.password" );
        databaseString = config.getString( "mysql.database" );

        database = new Database( plugin, host, user, password, databaseString );
        database.connect( );
        database.createTable( );
    }

    //<editor-fold desc="closeConnection">
    public void closeConnection( ) {
        database.disconnect( );
    }
    //</editor-fold>

    //<editor-fold desc="getDatabase">
    public Database getDatabase( ) {
        return database;
    }
    //</editor-fold>
}

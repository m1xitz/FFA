package de.m0xitz.ffa.database;

import com.google.common.collect.Maps;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import de.m0xitz.ffa.Main;
import de.m0xitz.ffa.user.inventory.Inventory;
import de.m0xitz.ffa.user.stats.Stats;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class Database {

    private Main plugin;

    private String host;
    private String user;
    private String password;
    private String database;

    private HikariDataSource dataSource;
    private ExecutorService pool;

    public Database( Main plugin, String host, String user, String password, String database ) {
        this.plugin = plugin;
        this.host = host;
        this.user = user;
        this.password = password;
        this.database = database;

        pool = Executors.newFixedThreadPool( 10 );
    }

    //<editor-fold desc="connect">
    public void connect( ) {
        if ( isConnected( ) )
            return;
        HikariConfig hikariConfig = new HikariConfig( );
        hikariConfig.setJdbcUrl( "jdbc:mysql://" + host + ":3306" );
        hikariConfig.setUsername( user );
        hikariConfig.setPassword( password );
        hikariConfig.setCatalog( database );
        hikariConfig.addDataSourceProperty( "cachePrepStmts", "true" );
        hikariConfig.addDataSourceProperty( "prepStmtCacheSize", "250" );
        hikariConfig.addDataSourceProperty( "prepStmtCacheSqlLimit", "2048" );
        hikariConfig.addDataSourceProperty( "useServerPrepStmts", "true" );
        hikariConfig.setMaximumPoolSize( 5 );

        dataSource = new HikariDataSource( hikariConfig );

        pool.execute( ( ) -> {
            Connection connection = getConnection( );
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement( "CREATE DATABASE IF NOT EXISTS " + database );
                preparedStatement.executeUpdate( );
                hikariConfig.setCatalog( database );
            } catch ( SQLException ex ) {
                ex.printStackTrace( );
            } finally {
                try {
                    preparedStatement.close( );
                    connection.close( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                }
            }
        } );
    }
    //</editor-fold>

    //<editor-fold desc="disconnect">
    public void disconnect( ) {
        if ( isConnected( ) )
            return;
        dataSource.close( );
    }
    //</editor-fold>

    //<editor-fold desc="isConnected">
    public boolean isConnected( ) {
        return dataSource != null && !dataSource.isClosed( );
    }
    //</editor-fold>

    //<editor-fold desc="getConnection">
    private Connection getConnection( ) {
        try {
            return isConnected( ) ? dataSource.getConnection( ) : null;
        } catch ( SQLException e ) {
            return null;
        }
    }
    //</editor-fold>

    //<editor-fold desc="createTable">
    public void createTable( ) {
        pool.execute( ( ) -> {
            Connection connection = getConnection( );
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement( "CREATE TABLE IF NOT EXISTS ffa_stats (UUID VARCHAR (100), KILLS VARCHAR (100), DEATHS VARCHAR (100), POINTS VARCHAR (100), JOINS VARCHAR (100), HIGHKILLSTREAK VARCHAR (100))" );
                preparedStatement.executeUpdate( );

                preparedStatement = connection.prepareStatement( "CREATE TABLE IF NOT EXISTS ffa_inventory (UUID VARCHAR (100), SWORD INTEGER, ROD INTEGER, BOW INTEGER, COMPASS INTEGER, FNS INTEGER)" );
                preparedStatement.executeUpdate( );
            } catch ( SQLException ex ) {
                ex.printStackTrace( );
            } finally {
                try {
                    preparedStatement.close( );
                    connection.close( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                }
            }
        } );
    }
    //</editor-fold>

    //<editor-fold desc="getStats">
    public void getStats( UUID uuid, Consumer< Stats > statsConsumer ) {
        pool.execute( ( ) -> {
            Connection connection = getConnection( );
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            long[] kills = { 0 };
            long[] deaths = { 0 };
            long[] points = { 100 };
            long[] joins = { 0 };
            long[] highKillstreak = { 0 };

            try {
                preparedStatement = connection.prepareStatement( "SELECT * FROM ffa_stats WHERE UUID = ?" );
                preparedStatement.setString( 1, uuid.toString( ) );
                resultSet = preparedStatement.executeQuery( );
                if ( resultSet.next( ) ) {
                    kills[0] = Long.parseLong( resultSet.getString( "KILLS" ) );
                    deaths[0] = Long.parseLong( resultSet.getString( "DEATHS" ) );
                    points[0] = Long.parseLong( resultSet.getString( "POINTS" ) );
                    joins[0] = Long.parseLong( resultSet.getString( "JOINS" ) );
                    highKillstreak[0] = Long.parseLong( resultSet.getString( "HIGHKILLSTREAK" ) );
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace( );
            } finally {
                try {
                    resultSet.close( );
                    preparedStatement.close( );
                    connection.close( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                }
            }
            statsConsumer.accept( new Stats( ) {
                @Override
                public long getKills( ) {
                    return kills[0];
                }

                @Override
                public long getDeaths( ) {
                    return deaths[0];
                }

                @Override
                public long getPoints( ) {
                    return points[0];
                }

                @Override
                public long getJoins( ) {
                    return joins[0];
                }

                @Override
                public long getHighestKillstreak( ) {
                    return highKillstreak[0];
                }
            } );
        } );
    }
    //</editor-fold>

    //<editor-fold desc="setStats">
    public void setStats( UUID uuid, Stats stats ) {
        pool.execute( ( ) -> {
            hasStats( uuid, has -> {
                Connection connection = getConnection( );
                PreparedStatement preparedStatement = null;
                try {
                    if ( has ) {
                        preparedStatement = connection.prepareStatement( "UPDATE ffa_stats SET KILLS = ?, DEATHS = ?, POINTS = ?, JOINS = ?, HIGHKILLSTREAK = ? WHERE UUID = ?" );
                        preparedStatement.setString( 1, Long.toString( stats.getKills( ) ) );
                        preparedStatement.setString( 2, Long.toString( stats.getDeaths( ) ) );
                        preparedStatement.setString( 3, Long.toString( stats.getPoints( ) ) );
                        preparedStatement.setString( 4, Long.toString( stats.getJoins( ) ) );
                        preparedStatement.setString( 5, Long.toString( stats.getHighestKillstreak( ) ) );
                        preparedStatement.setString( 6, uuid.toString( ) );
                    } else {
                        preparedStatement = connection.prepareStatement( "INSERT INTO ffa_stats (UUID, KILLS, DEATHS, POINTS, JOINS, HIGHKILLSTREAK) VALUES (?, ?, ?, ?, ?, ?)" );
                        preparedStatement.setString( 1, uuid.toString( ) );
                        preparedStatement.setString( 2, Long.toString( stats.getKills( ) ) );
                        preparedStatement.setString( 3, Long.toString( stats.getDeaths( ) ) );
                        preparedStatement.setString( 4, Long.toString( stats.getPoints( ) ) );
                        preparedStatement.setString( 5, Long.toString( stats.getJoins( ) ) );
                        preparedStatement.setString( 6, Long.toString( stats.getHighestKillstreak( ) ) );
                    }
                    preparedStatement.executeUpdate( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                } finally {
                    try {
                        preparedStatement.close( );
                        connection.close( );
                    } catch ( SQLException ex ) {
                        ex.printStackTrace( );
                    }
                }
            } );
        } );
    }
    //</editor-fold>

    //<editor-fold desc="hasStats">
    public void hasStats( UUID uuid, Consumer< Boolean > booleanConsumer ) {
        pool.execute( ( ) -> {
            boolean has = false;
            Connection connection = getConnection( );
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = connection.prepareStatement( "SELECT UUID FROM ffa_stats WHERE UUID = ?" );
                preparedStatement.setString( 1, uuid.toString( ) );
                resultSet = preparedStatement.executeQuery( );
                while ( resultSet.next( ) ) {
                    has = true;
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace( );
            } finally {
                try {
                    resultSet.close( );
                    preparedStatement.close( );
                    connection.close( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                }
            }
            booleanConsumer.accept( has );
        } );
    }
    //</editor-fold>

    //<editor-fold desc="getRanking">
    public HashMap< UUID, Integer > getRanking( ) {
        HashMap< UUID, Integer > map = Maps.newHashMap( );
        Connection connection = getConnection( );
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement( "SELECT * FROM ffa_stats ORDER BY POINTS DESC" );
            resultSet = preparedStatement.executeQuery( );
            int i = 1;
            while ( resultSet.next( ) ) {
                map.put( UUID.fromString( resultSet.getString( "UUID" ) ), i );
                i++;
            }
        } catch ( SQLException ex ) {
            ex.printStackTrace( );
        } finally {
            try {
                resultSet.close( );
                preparedStatement.close( );
                connection.close( );
            } catch ( SQLException ex ) {
                ex.printStackTrace( );
            }
        }
        return map;
    }

    public int getRanking( UUID uuid ) {
        return getRanking( ).getOrDefault( uuid, -1 );
    }
    //</editor-fold>

    //<editor-fold desc="getInventory">
    public void getInventory( UUID uuid, Consumer< Inventory > inventoryConsumer ) {
        pool.execute( ( ) -> {
            Connection connection = getConnection( );
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            int[] sword = { 0 };
            int[] rod = { 1 };
            int[] bow = { 2 };
            int[] compass = { 7 };
            int[] fns = { 8 };

            try {
                preparedStatement = connection.prepareStatement( "SELECT * FROM ffa_inventory WHERE UUID = ?" );
                preparedStatement.setString( 1, uuid.toString( ) );
                resultSet = preparedStatement.executeQuery( );
                if ( resultSet.next( ) ) {
                    sword[0] = resultSet.getInt( "SWORD" );
                    rod[0] = resultSet.getInt( "ROD" );
                    bow[0] = resultSet.getInt( "BOW" );
                    compass[0] = resultSet.getInt( "COMPASS" );
                    fns[0] = resultSet.getInt( "FNS" );
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace( );
            } finally {
                try {
                    resultSet.close( );
                    preparedStatement.close( );
                    connection.close( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                }
            }
            inventoryConsumer.accept( new Inventory( ) {
                @Override
                public int getSword( ) {
                    return sword[0];
                }

                @Override
                public int getRod( ) {
                    return rod[0];
                }

                @Override
                public int getBow( ) {
                    return bow[0];
                }

                @Override
                public int getCompass( ) {
                    return compass[0];
                }


                @Override
                public int getFns( ) {
                    return fns[0];
                }
            } );
        } );
    }
    //</editor-fold>

    //<editor-fold desc="setInventory">
    public void setInventory( UUID uuid, Inventory inventory ) {
        pool.execute( ( ) -> {
            hasInventory( uuid, has -> {
                Connection connection = getConnection( );
                PreparedStatement preparedStatement = null;
                try {
                    if ( has ) {
                        preparedStatement = connection.prepareStatement( "UPDATE ffa_inventory SET SWORD = ?, ROD = ?, BOW = ?, COMPASS = ?, FNS = ? WHERE UUID = ?" );
                        preparedStatement.setInt( 1, inventory.getSword( ) );
                        preparedStatement.setInt( 2, inventory.getRod( ) );
                        preparedStatement.setInt( 3, inventory.getBow( ) );
                        preparedStatement.setInt( 4, inventory.getCompass( ) );
                        preparedStatement.setInt( 5, inventory.getFns( ) );
                        preparedStatement.setString( 6, uuid.toString( ) );
                    } else {
                        preparedStatement = connection.prepareStatement( "INSERT INTO ffa_inventory (UUID, SWORD, ROD, BOW, COMPASS, FNS) VALUES (?, ?, ?, ?, ?, ?)" );
                        preparedStatement.setString( 1, uuid.toString( ) );
                        preparedStatement.setInt( 2, inventory.getSword( ) );
                        preparedStatement.setInt( 3, inventory.getRod( ) );
                        preparedStatement.setInt( 4, inventory.getBow( ) );
                        preparedStatement.setInt( 5, inventory.getCompass( ) );
                        preparedStatement.setInt( 6, inventory.getFns( ) );
                    }
                    preparedStatement.executeUpdate( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                } finally {
                    try {
                        preparedStatement.close( );
                        connection.close( );
                    } catch ( SQLException ex ) {
                        ex.printStackTrace( );
                    }
                }
            } );
        } );
    }
    //</editor-fold>

    //<editor-fold desc="hasInventory">
    public void hasInventory( UUID uuid, Consumer< Boolean > booleanConsumer ) {
        pool.execute( ( ) -> {
            boolean has = false;
            Connection connection = getConnection( );
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            try {
                preparedStatement = connection.prepareStatement( "SELECT UUID FROM ffa_inventory WHERE UUID = ?" );
                preparedStatement.setString( 1, uuid.toString( ) );
                resultSet = preparedStatement.executeQuery( );
                while ( resultSet.next( ) ) {
                    has = true;
                }
            } catch ( SQLException ex ) {
                ex.printStackTrace( );
            } finally {
                try {
                    resultSet.close( );
                    preparedStatement.close( );
                    connection.close( );
                } catch ( SQLException ex ) {
                    ex.printStackTrace( );
                }
            }
            booleanConsumer.accept( has );
        } );
    }
    //</editor-fold>
}
